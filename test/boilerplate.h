//  boilerplate.h

#pragma once

/**
 * returns true or false if parsing passed or failed.
 * If a non parsing error occurs, errorMsg will be populated.
 * If a parsing error occurs, parseErrorMsg will be populated
 */
extern bool parseTest(const std::string &filename, std::string *errorMsg, std::string *parseErrorMsg);

/**
 * Runs parser test on filename. Asserts if parser should pass or fail
 */
#define LUCY_PARSE_TEST(filename, shouldPass) \
{\
  std::string error = "";\
  std::string parseError = "";\
  bool parseResults = parseTest((filename), &error, &parseError);\
  INFO(filename);\
  INFO(error);\
  REQUIRE(error.size() == 0);\
  if (shouldPass == true) {\
  REQUIRE(parseError.size() == 0);\
  INFO(parseError);\
  }\
  REQUIRE(parseResults == (shouldPass));\
}

/**
 * Runs the given Lucy program.
 * Returns if the program was run successfully, and after
 * return, exitStatus will have the return value of the program.
 */
extern bool ExecuteLucyProgram(std::string &path, int &exitStatus);

/**
 * Runs parser test on filename. Asserts return code
 */
#define LUCY_CODE_TEST(filename, returnCode) \
{\
  int returnVal;\
  int success = ExecuteLucyProgram((filename), returnVal);\
  INFO(filename);\
  REQUIRE(success);\
  REQUIRE(returnVal == (returnCode));\
}

/**
 * Runs a semantic check on the given file.
 * Returns if the test passed or failed.
 * If the test passed BEFORE the semantic check, errorMsg will be populated
 * If the semantic check fails, semanticErrorMsg will be populated.
 */
extern bool semanticTest(const std::string &filename, std::string *errorMsg, std::string *semanticErrorMsg);

/**
 * Runs semantic test on file, asserts should pass
 */
#define LUCY_SEMANTIC_TEST(filename, shouldPass)\
{\
  std::string errorMsg = "";\
  std::string semanticErrorMsg = "";\
  bool result = semanticTest(filename, &errorMsg, &semanticErrorMsg);\
  INFO(errorMsg);\
  REQUIRE(errorMsg.size() == 0);\
  if (shouldPass == false) {\
  CHECK(semanticErrorMsg.size() == 0);\
  INFO(semanticErrorMsg);\
  }\
  REQUIRE(result == shouldPass);\
}

#define LUCY_SEMANTIC_TEST_SHOULD_PASS(filename) LUCY_SEMANTIC_TEST(filename, true)
#define LUCY_SEMANTIC_TEST_SHOULD_FAIL(filename) LUCY_SEMANTIC_TEST(filename, false)
