//  unit-typechecker.cpp

#include "catch.h"

#include "lexer.h"
#include "token.h"
#include "parser.h"
#include "typechecker.h"
#include "filesource.h"
#include "exceptions.h"
#include "boilerplate.h"

/*------------------------------
 * Semantics Tests
 *------------------------------
 */

using namespace lcy;

TEST_CASE("Typecheck 1", "[Typechecker]") {
  std::string filename = std::string(__TEST__) + "test_files/typecheck1.txt";
  LUCY_SEMANTIC_TEST_SHOULD_FAIL(filename);
}

TEST_CASE("Typecheck 2", "[Typechecker]") {
  std::string filename = std::string(__TEST__) + "test_files/typecheck2.txt";
  LUCY_SEMANTIC_TEST_SHOULD_FAIL(filename)
}

TEST_CASE("Typecheck 3", "[Typechecker]") {
  std::string filename = std::string(__TEST__) + "test_files/typecheck3.txt";
  LUCY_SEMANTIC_TEST_SHOULD_PASS(filename);
}

TEST_CASE("Typecheck 4", "[Typechecker]") {
  std::string filename = std::string(__TEST__) + "test_files/typecheck4.txt";
  LUCY_SEMANTIC_TEST_SHOULD_FAIL(filename);
}

TEST_CASE("Typecheck 5", "[Typechecker]") {
  std::string filename = std::string(__TEST__) + "test_files/typecheck5.txt";
  LUCY_SEMANTIC_TEST_SHOULD_PASS(filename);
}

TEST_CASE("Typecheck 6", "[Typechecker]") {
  std::string filename = std::string(__TEST__) + "test_files/typecheck6.txt";
  LUCY_SEMANTIC_TEST_SHOULD_FAIL(filename);
}

TEST_CASE("Typecheck 7", "[Typechecker]") {
  std::string filename = std::string(__TEST__) + "test_files/typecheck7.txt";
  LUCY_SEMANTIC_TEST_SHOULD_PASS(filename);
}

TEST_CASE("Typecheck 8", "[Typechecker]") {
  std::string filename = std::string(__TEST__) + "test_files/typecheck8.txt";
  LUCY_SEMANTIC_TEST_SHOULD_FAIL(filename);
}

TEST_CASE("Typecheck 9", "[Typechecker]") {
  std::string filename = std::string(__TEST__) + "test_files/typecheck9.txt";
  LUCY_SEMANTIC_TEST_SHOULD_FAIL(filename);
}

TEST_CASE("Typecheck Label Use", "[Typechecker]") {
  std::string filename = std::string(__TEST__) + "test_files/typecheck_label_use.txt";
  LUCY_SEMANTIC_TEST_SHOULD_FAIL(filename);
}

TEST_CASE("Typecheck Prefix op address operator on rvalue", "[Typechecker]") {
  std::string filename = std::string(__TEST__) + "test_files/prefix_op_typecheck/address_op_rvalue.txt";
  LUCY_SEMANTIC_TEST_SHOULD_FAIL(filename);
}

TEST_CASE("Typecheck Negating an array", "[Typechecker]") {
  std::string filename = std::string(__TEST__) + "test_files/prefix_op_typecheck/negate_array.txt";
  LUCY_SEMANTIC_TEST_SHOULD_FAIL(filename);
}

TEST_CASE("Typecheck Dereferencing non-ptr", "[Typechecker]") {
  std::string filename = std::string(__TEST__) + "test_files/prefix_op_typecheck/dereference_non_ptr.txt";
  LUCY_SEMANTIC_TEST_SHOULD_FAIL(filename);
}

TEST_CASE("Typecheck bad dereference assign", "[Typechecker]") {
  std::string filename = std::string(__TEST__) + "test_files/typecheck_bad_pointer_assign.txt";
  LUCY_SEMANTIC_TEST_SHOULD_FAIL(filename);
}
