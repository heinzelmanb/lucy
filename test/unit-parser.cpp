//  unit-parser.cpp

#include "catch.h"

#include "lexer.h"
#include "token.h"
#include "parser.h"
#include "typechecker.h"
#include "filesource.h"
#include "exceptions.h"
#include <string>
#include "boilerplate.h"

/*------------------------------
 * Parser Tests
 *------------------------------
 */

TEST_CASE("Parser", "[Parser]")
{
  SECTION("Statement List") {
    std::string filename = std::string(__TEST__) + "test_files/parser_statement_list.txt";
    LUCY_PARSE_TEST(filename, true);
  }
  
  SECTION("Lucy Label") {
    std::string filename = std::string(__TEST__) + "test_files/parser_label.txt";
    LUCY_PARSE_TEST(filename, true);
  }
  
  SECTION("Parser Array Access") {
    std::string filename = std::string(__TEST__) + "test_files/parser_array_access.txt";
    LUCY_PARSE_TEST(filename, true);
  }
  
  SECTION("Parser Array Access 2") {
    std::string filename = std::string(__TEST__) + "test_files/parser_array_access2.txt";
    LUCY_PARSE_TEST(filename, false);
  }
  
  SECTION("If Statement") {
    std::string filename = std::string(__TEST__) + "test_files/parser_if_statement.txt";
    LUCY_PARSE_TEST(filename, true);
  }
  
  SECTION("Goto Statement") {
    std::string filename = std::string(__TEST__) + "test_files/goto.txt";
    LUCY_PARSE_TEST(filename, true);
  }
  
  SECTION("Call Statement 1") {
    std::string filename = std::string(__TEST__) + "test_files/parser_call_statement.txt";
    LUCY_PARSE_TEST(filename, true);
  }
  
  SECTION("Call Statement 2") {
    std::string filename = std::string(__TEST__) + "test_files/parser_call_statement2.txt";
    LUCY_PARSE_TEST(filename, true);
  }
}


