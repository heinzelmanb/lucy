//  boilerplate.cpp

#include "catch.h"

#include "filesource.h"
#include "parser.h"
#include "lexer.h"
#include "exceptions.h"
#include "compiler.h"
#include "typechecker.h"

#include <iostream>

bool parseTest(const std::string &filename, std::string *errorMsg, std::string *parseErrorMsg)
{
  std::ifstream input(filename);
  if (input.is_open() == false) {
    *errorMsg = "Could not open file";
    return false;
  }
  lcy::FileSource fileSource(&input);
  lcy::Lexer lex(std::make_shared<lcy::FileSource>(fileSource));
  lcy::Parser parser(std::make_shared<lcy::Lexer>(lex));
  try {
    parser.Parse();
  } catch (lcy::ParseError parseError) {
    *parseErrorMsg = std::string(parseError.what());
    return false;
  }
  return true;
}

static int executeSysCommand(std::string cmd) {
  int result = std::system(cmd.c_str());
  return WEXITSTATUS(result);
}

bool ExecuteLucyProgram(std::string &path, int &exitStatus) {
  std::unique_ptr<lcy::CompilerOptions> options = std::unique_ptr<lcy::CompilerOptions>(new lcy::CompilerOptions);
  
  std::string outputLucyFilePath = path + ".s";
  
  options->outputToStdOut = false;
  options->writeToFile = true;
  options->outputFile = outputLucyFilePath;
  
  lcy::Compiler compiler(std::move(options));
  bool success = compiler.Compile(path);
  
  if (success == false) {
    std::cerr << "Could not compile lucy file" << std::endl;
    return false;
  }
  
  int returnValue = executeSysCommand(std::string("gcc ") + outputLucyFilePath);
  if (returnValue != 0) {
    std::cerr << "Could not assemble lucy file" << std::endl;
    return false;
  }
  
  returnValue = executeSysCommand("./a.out");
  
  std::remove("a.out");
  std::remove(outputLucyFilePath.c_str());
  
  exitStatus = returnValue;
  
  return true;
}

bool semanticTest(const std::string &filename, std::string *errorMsg, std::string *semanticErrorMsg)
{
  std::ifstream input(filename);
  
  if (input.is_open() == false) {
    *errorMsg = "Could not open file";
    return false;
  }
  
  lcy::FileSource fileSource(&input);
  lcy::Lexer lex(std::make_shared<lcy::FileSource>(fileSource));
  lcy::Parser parser(std::make_shared<lcy::Lexer>(lex));
  lcy::TypeChecker typeChecker;
  
  try {
    auto program = parser.Parse();
    typeChecker.CheckType(program.get());
  } catch (lcy::ParseError parseError) {
    *errorMsg = std::string(parseError.what());
    return false;
  } catch (lcy::TypeError typeError) {
    return false;
  }
  return true;
}

