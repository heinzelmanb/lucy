// unit-codegen.cpp

#include "catch.h"
#include "compiler.h"
#include "util.h"

#include "boilerplate.h"

#include <string>

/*------------------------------
 * Code Generation Tests
 *------------------------------
 */

TEST_CASE("CodeGen", "[Codegenerator]") {
  
  SECTION("BASIC CODE GENERATION") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/basic.txt";
    LUCY_CODE_TEST(filename, 1);
  }
  
  SECTION("BASIC ASSIGNMENT") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/assignment.txt";
    LUCY_CODE_TEST(filename, 2);
  }
  
  SECTION("BASIC FUNCTION CALL") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/functioncall.txt";
    LUCY_CODE_TEST(filename, 4);
  }
  
  SECTION("Equals") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/equals.txt";
    LUCY_CODE_TEST(filename, 0);
  }
  
  SECTION("Equals2") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/equals2.txt";
    LUCY_CODE_TEST(filename, 1);
  }
  
  SECTION("Not Equal") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/notequal.txt";
    LUCY_CODE_TEST(filename, 1);
  }
  
  SECTION("Not Equal") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/notequal2.txt";
    LUCY_CODE_TEST(filename, 0);
  }
  
  SECTION("IF STATEMENT") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/ifstatement.txt";
    LUCY_CODE_TEST(filename, 2);
  }
  
  SECTION("IF STATEMENT2") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/ifstatement2.txt";
    LUCY_CODE_TEST(filename, 2);
  }
  
  SECTION("IF STATEMENT3") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/ifstatement3.txt";
    LUCY_CODE_TEST(filename, 2);
  }
  
  SECTION("IF STATEMENT4") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/ifstatement4.txt";
    LUCY_CODE_TEST(filename, 1);
  }
  
  SECTION("GreaterThan") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/greaterthan.txt";
    LUCY_CODE_TEST(filename, 1);
  }
  
  SECTION("GreaterThan2") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/greaterthan2.txt";
    LUCY_CODE_TEST(filename, 1);
  }
  
  SECTION("GreaterThan3") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/greaterthan3.txt";
    LUCY_CODE_TEST(filename, 1);
  }
  
  SECTION("LessThan") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/lessthan.txt";
    LUCY_CODE_TEST(filename, 1);
  }
  
  SECTION("LessThan2") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/lessthan2.txt";
    LUCY_CODE_TEST(filename, 1);
  }
  
  SECTION("GreaterThanEqual") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/greaterthanequal.txt";
    LUCY_CODE_TEST(filename, 1);
  }
  
  SECTION("GreaterThanEqual2") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/greaterthanequal2.txt";
    LUCY_CODE_TEST(filename, 1);
  }
  
  SECTION("LessThanEqual") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/lessthanequal.txt";
    LUCY_CODE_TEST(filename, 1);
  }
  
  SECTION("LessThanEqual2") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/lessthanequal2.txt";
    LUCY_CODE_TEST(filename, 1);
  }
  
  SECTION("Goto") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/goto.txt";
    LUCY_CODE_TEST(filename, 1);
  }
  
  SECTION("Multiply") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/multiply.txt";
    LUCY_CODE_TEST(filename, 17);
  }
  
  SECTION("Array Assign/Get") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/array_assign_get.txt";
    LUCY_CODE_TEST(filename, 50);
  }
  
  SECTION("Array Assign/Get") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/array_variable_index.txt";
    LUCY_CODE_TEST(filename, 5);
  }
  
  SECTION("Negate immediate value") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/negate_immediate_value.txt";
    LUCY_CODE_TEST(filename, 0);
  }
  
  SECTION("Negate id") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/negate_id.txt";
    LUCY_CODE_TEST(filename, 0);
  }
  
  SECTION("swap") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/swap.txt";
    LUCY_CODE_TEST(filename, 8);
  }
  
  SECTION("swap2") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/swap2.txt";
    LUCY_CODE_TEST(filename, 6);
  }
  
  SECTION("Pointer as parameter") {
    std::string filename = std::string(__TEST__) + "test_files/codegen/pointer_parameter.txt";
    LUCY_CODE_TEST(filename, 2);
  }
}
