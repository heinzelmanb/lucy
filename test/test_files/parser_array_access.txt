func main() ~ int32_t
begin

    decl int32_t[10] i;
    i[0] = i[1];

    return i;
end
