 func swap(decl int32_t *a, decl int32_t *b) ~ int32_t
 begin
     decl int32_t temp;
     temp = *a;
     *a = *b;
     *b = temp;
     return 0;
 end

 func main() ~ int32_t
 begin
     decl int32_t a;
     decl int32_t b;
     decl int32_t *aPtr;
     decl int32_t *bPtr;

     a = 5;
     b = 8;

     aPtr = &a;
     bPtr = &b;

     call swap(aPtr, bPtr);
     return a;
 end
