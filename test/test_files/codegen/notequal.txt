func main() ~ int32_t
begin
    decl int32_t i;

    i = 2;

    i = i != 1;

    return i;
end
