func main() ~ int32_t
begin
    decl int32_t i;
    decl int32_t j;

    j = 2;

    i = -j;

    j = j + i;

    return j;
end
