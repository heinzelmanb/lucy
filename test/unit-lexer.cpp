//  unit-lexer.cpp

#include "catch.h"

#include "lexer.h"
#include "token.h"
#include "filesource.h"

/*------------------------------
 * Lexer Tests
 *------------------------------
 */

using namespace lcy;

TEST_CASE("Lexer", "[Lexer]") {
  std::string filename = std::string(__TEST__) + "test_files/lexer.txt";
  std::ifstream input(filename);
  
  SECTION("Opening File") {
    REQUIRE(input.is_open());
  }
  
  lcy::FileSource fileSource(&input);
  
  lcy::Lexer lex(std::make_shared<lcy::FileSource>(fileSource));
  SECTION("Recognizing Tokens") {
    REQUIRE(TokenType::Add == lex.GetNextToken().GetType());
    REQUIRE(TokenType::Subtract == lex.GetNextToken().GetType());
    REQUIRE(TokenType::Multiply == lex.GetNextToken().GetType());
    REQUIRE(TokenType::Divide == lex.GetNextToken().GetType());
    REQUIRE(TokenType::Equals == lex.GetNextToken().GetType());
    REQUIRE(TokenType::GreaterThanEqual == lex.GetNextToken().GetType());
    REQUIRE(TokenType::LessThanEqual == lex.GetNextToken().GetType());
    REQUIRE(TokenType::LogicalOr == lex.GetNextToken().GetType());
    
    auto tok = lex.GetNextToken();
    REQUIRE(TokenType::Id == tok.GetType());
    REQUIRE(tok.GetValue() == "identifier");
    
    REQUIRE(TokenType::Int32Keyword == lex.GetNextToken().GetType());
    
    auto integerToken = lex.GetNextToken();
    REQUIRE(integerToken.GetType() == TokenType::Integer32);
    REQUIRE(integerToken.GetValue() == "100");
    
    REQUIRE(TokenType::BeginKeyword == lex.GetNextToken().GetType());
    REQUIRE(TokenType::EndKeyword == lex.GetNextToken().GetType());
    REQUIRE(TokenType::Comma == lex.GetNextToken().GetType());
    REQUIRE(TokenType::LeftParen == lex.GetNextToken().GetType());
    REQUIRE(TokenType::RightParen == lex.GetNextToken().GetType());
    
    REQUIRE(TokenType::Tilde == lex.GetNextToken().GetType());
    REQUIRE(TokenType::DeclKeyword == lex.GetNextToken().GetType());
    REQUIRE(TokenType::ReturnKeyword == lex.GetNextToken().GetType());
    REQUIRE(TokenType::CallKeyword == lex.GetNextToken().GetType());
    REQUIRE(TokenType::At == lex.GetNextToken().GetType());
    REQUIRE(TokenType::NotEqual == lex.GetNextToken().GetType());
    
    // this must be the last check
    REQUIRE(TokenType::Eof == lex.GetNextToken().GetType());
  }
}

TEST_CASE("Line Number", "[Lexer]") {
  std::string filename = std::string(__TEST__) + "test_files/line_number.txt";
  std::ifstream input(filename);
  
  SECTION("Opening File") {
    REQUIRE(input.is_open());
  }
  lcy::FileSource fileSource(&input);
  lcy::Lexer lex(std::make_shared<lcy::FileSource>(fileSource));
  auto tok = lex.GetNextToken();
  REQUIRE(tok.GetLineNumber() == 0);
  
  tok = lex.GetNextToken();
  REQUIRE(tok.GetLineNumber() == 0);
  
  tok = lex.GetNextToken();
  REQUIRE(tok.GetLineNumber() == 1);
  
  tok = lex.GetNextToken();
  REQUIRE(tok.GetLineNumber() == 2);
  
  tok = lex.GetNextToken();
  lex.PeekNextToken();
  REQUIRE(tok.GetLineNumber() == 4);
  
  tok = lex.GetNextToken();
  REQUIRE(tok.GetLineNumber() == 7);
  
  REQUIRE(lex.GetNextToken().GetType() == TokenType::Eof);
  
}
