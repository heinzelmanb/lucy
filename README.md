# Lucy

Lucy is an IR (Intermediate Representation) Language. Lucy is a low level abstraction over assembly language. 

## To Build:
- You will have to have `cmake` installed.
- `mkdir bin && cmake ../`
