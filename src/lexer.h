#pragma once

#include "token.h"
#include "programsource.h"

#include <map>
#include <string>
#include <fstream>
#include <memory>

namespace lcy {

class Lexer 
{
  public:
  Lexer(std::shared_ptr<lcy::ProgramSource> input);
	~Lexer();
  
	const Token GetNextToken();
  const Token GetCurrentToken();
  const Token PeekNextToken();
  
private:
  std::shared_ptr<lcy::ProgramSource> gInput;
  static std::map<std::string, TokenType> operatorMap;
  static std::map<std::string, TokenType> keywordMap;
  static std::map<std::string, TokenType> punctuationMap;
  
  lcy::Token gPeekToken;
  lcy::Token gCurrentToken;
  
private:
  void        skipWs();
  const char  nextChar();
  const char  peekChar();
  bool        isOperator();
  bool        isPunctuation();
  int         getLineNumber() const;
  
  const Token getTextToken();
  const Token getOperatorToken();
  const Token getIntegerToken();
  const Token getPunctuationToken();
};

}
