#pragma once

#include <string>
#include <memory>

#include "types.h"

namespace lcy
{
  namespace x64
  {
    enum OpSize {
      OpSizeNone = 0,
      OpSizeByte = 1,
      OpSizeWord = 2,
      OpSizeDoubleWord = 4,
      OpSizeQuadWord = 8
    };

    class Operand
    {
    public:
      virtual std::string ToString() const = 0;
      virtual size_t GetSize() const = 0;
      virtual bool IsRegister() const = 0;
    };

    class Register : public Operand
    {
    private:
      Register(std::string quadWordReg, std::string doublWordReg, std::string wordReg, std::string byteReg, OpSize size);
    public:
      // Returns 64 bit version of the register
      // e.g. eax -> rax
      std::shared_ptr<Register> GetQuadWordRegister() const;
      std::shared_ptr<Register> GetDoubleWordRegister() const;
      std::shared_ptr<Register> GetWordRegister() const;
      std::shared_ptr<Register> GetByteRegister() const;
      
      std::string ToString() const;
      
      std::string GetName() const;
      
      size_t GetSize() const;
      
      std::shared_ptr<Register> RegisterForSize(OpSize size) const;
      
      virtual bool IsRegister() const {
        return true;
      }
      
      std::shared_ptr<Register> GetShared() const;
      
      bool IsEqual(const std::shared_ptr<Register> other) const;
      
    private:
      std::string                 gQuadWordRegisterName;
      std::string                 gDoubleWordRegisterName;
      std::string                 gWordRegisterName;
      std::string                 gByteRegisterName;
      OpSize                      gRegisterSize;
      
    public:
      // 64 BIT Registers
      static Register Rax;
      static Register Rcx;
      static Register Rdx;
      static Register Rbx;
      static Register Rsi;
      static Register Rdi;
      static Register Rsp;
      static Register Rbp;
      static Register R8;
      static Register R9;
    };
    
    // ex $0
    class ImmediateOperand : public Operand
    {
    public:
      ImmediateOperand(Int64 value, size_t size);
      
      Int64 GetValue() const;
      
      std::string ToString() const;
      
      size_t GetSize() const;
      
      virtual bool IsRegister() const {
        return false;
      }
    private:
      Int64 gValue;
      size_t gSize;
    };
    
    // ex. -8(%ebp)
    class BaseRelativeOperand : public Operand
    {
    public:
      BaseRelativeOperand(std::shared_ptr<Register> reg, int offset, size_t size);
      
      std::string ToString() const;
      
      int GetOffset() const;
      
      size_t GetSize() const;
      
      virtual bool IsRegister() const {
        return false;
      }
      
      std::shared_ptr<Register> GetRegister() const;
    private:
      int gOffset;
      size_t gSize;
      std::shared_ptr<Register> gRegister;
    };
    
     // examle (%rax, %rcx, 8) (arrayAddress, offset, sizeof)
    class ArrayAccessOperand : public Operand
    {
    public:
      ArrayAccessOperand(std::shared_ptr<Register> arrayAddress, std::shared_ptr<Register> offsetReg, size_t size);
      
      std::string ToString() const;
      
      std::shared_ptr<Register> GetOffsetRegister() const;
      
      std::shared_ptr<Register> GetArrayAddressRegister() const;
      
      /**
       * Size refers to the size of the underlying base type. Thus for an int32_t array of size 10, size would
       * still be sizeof(int32_t)
       */
      size_t GetSize() const;
      
      virtual bool IsRegister() const {
        return false;
      }
      
    private:
      std::shared_ptr<Register> gArrayAddress;
      std::shared_ptr<Register> gOffsetRegister;
      size_t gElementSize;
      
    };
  }
}
