#pragma once

#include <memory>
#include <string>
#include <vector>
#include <map>
#include <set>

#include "visitor.h"
#include "ast.h"
#include "symboltable.h"

namespace lcy
{
  
class TypeChecker : public Visitor
{
public:
  TypeChecker();
  
  bool CheckType(Program *program);
  
  virtual void Visit(Program                          *program);
  virtual void Visit(FunctionDeclaration              *functionDecl);
  virtual void Visit(StatementList                    *statementList);
  virtual void Visit(IdentifierAddress                *address);
  virtual void Visit(AssignmentStatement              *statement);
  virtual void Visit(VariableInitializationStatement  *statement);
  virtual void Visit(ParameterList                    *parameterList);
  virtual void Visit(Int32Address                     *address);
  virtual void Visit(ReturnStatement                  *statement);
  virtual void Visit(CallStatement                    *statement);
  virtual void Visit(SimpleExpression                 *simpleExpression);
  virtual void Visit(ComplexExpression                *complexExpression);
  virtual void Visit(LucyLabel                        *label);
  virtual void Visit(IfStatement                      *statement);
  virtual void Visit(GotoStatement                    *statement);
  
private:
  std::set<std::string> gDefinedLabels;
  
  std::set<std::string> gLabelsInUse;
  
  SymbolTableFrame gDefinedVars;
  std::map<std::string, FunctionDeclaration *> gFunctionLut;
  lcy::FunctionDeclaration *gCurrentFunction;
  std::unique_ptr<lcy::Type> gLastType;
};
  
}
