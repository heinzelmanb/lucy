//  util.cpp

#include "util.h"

static int executeSysCommand(std::string cmd) {
  int result = std::system(cmd.c_str());
  return WEXITSTATUS(result);
}

bool ExecuteAssembly(std::string path, int &exitStatus)
{
  int returnValue = executeSysCommand(std::string("gcc ") + path);
  if (returnValue != 0) {
    std::cerr << "Could not assemble lucy file" << std::endl;
    return false;
  }
  
  returnValue = executeSysCommand("./a.out");
  
  std::remove("a.out");
  
  exitStatus = returnValue;
  
  return true;
}

