#pragma once

#include <string>
#include <memory>
#include <map>
#include <stack>

#include "ast.h"

namespace lcy {

  // private Class to hold state for local vars
  class VariableData
  {
  public:
    VariableData() {}
    VariableData(Type *type)
    {
      gType = type;
      gStackOffset = 0;
      gSize = 0;
    }
    
    VariableData(size_t stackOffset, size_t size, Type *type)
    {
      gSize = size;
      gStackOffset = stackOffset;
      gType = type;
    }
    
    // offset from basepointer
    size_t GetOffset() const { return gStackOffset; }
    size_t GetSize() const { return gSize; }
    const Type *GetType() const { return gType; }
  private:
    size_t gStackOffset;
    size_t gSize;
    Type *gType;
  };
  
  class SymbolTableFrame
  {
  public:
    void AddDataForId(const std::string id, std::shared_ptr<VariableData> data);
    
    VariableData *GetDataForId(const std::string id) const;
    
    bool IdExists(const std::string id) const;
    
    void Clear();
    
  private:
    std::map<std::string, std::shared_ptr<VariableData>> gCache;
  };
  
  class SymbolTable
  {
  public:
    SymbolTable();
    SymbolTableFrame *TopFrame();
    
    void PopFrame();
    
    void PushFrame();
    
  private:
    std::stack<std::shared_ptr<SymbolTableFrame>> gFrames;
  };
  
}
