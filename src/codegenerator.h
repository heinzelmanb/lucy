#pragma once

#include <memory>

#include "ast.h"
#include "asm.h"
#include "visitor.h"

#ifdef __x86_64
#include "x64codegenerator.h"

namespace lcy
{
typedef lcy::x64::x64CodeGenerator CodeGenerator;
}

#else
#error("Archetecture not supported")
#endif
