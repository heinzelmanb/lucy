#pragma once

#include <vector>
#include <string>
#include <memory>

#include "token.h"
#include "visitor.h"
#include "object.h"
#include "types.h"

namespace lcy
{
  class Declaration;
  class ParameterList;
  class Type;
  class StatementList;
  class Statement;
  class Address;
  class IdentifierAddress;
  class VariableInitializationStatement;
  class SimpleExpression;
  class ComplexExpression;
  class Expression;
  
  class Node
  {
  public:
    virtual void Accept(Visitor *visitor)=0;
  };
  
  class Program : public Node
  {
  public:
    Program();
    Program(Program &&other);
    
    void AppendDeclaration(std::unique_ptr<Declaration> decl);
    void AppendDeclarationList(std::unique_ptr<Program> program);
    
    std::vector<Declaration *> GetDeclarationList() const;
    
    void Accept(Visitor *visitor);
  private:
    std::vector<std::unique_ptr<Declaration>> gDeclList;
  };
  
  class Declaration : Node
  {
  public:
    Declaration() {}
    Declaration(Declaration &&other) {}
    virtual void Accept(Visitor *visitor)=0;
  };
  
  class FunctionDeclaration : public Declaration
  {
  public:
    FunctionDeclaration(  std::unique_ptr<IdentifierAddress>         id,
                          std::unique_ptr<ParameterList>  parameterList,
                          std::unique_ptr<Type>           returnType,
                          std::unique_ptr<StatementList>  statementList);
    
    FunctionDeclaration(FunctionDeclaration &&other);
    
    StatementList *GetStatementList();
    
    IdentifierAddress *GetFunctionIdentifier() const;
    
    Type *GetReturnType() const;
    
    ParameterList *GetParameterList() const;
    
    void Accept(Visitor *visitor);
  private:
    std::unique_ptr<IdentifierAddress>         gId;
    std::unique_ptr<ParameterList>  gParameterList;
    std::unique_ptr<Type>           gReturnType;
    std::unique_ptr<StatementList>  gStatementList;
  };
  
  class Type
  {
  public:
    virtual size_t GetTypeSize() const = 0;
    virtual bool IsEqual(Type *other) const = 0;
    virtual std::unique_ptr<Type> Clone() const = 0;
    virtual bool IsReferenceType() const
    {
      return false;
    }
    
    virtual bool IsStackArray() const
    {
      return false;
    }
  };
  
  class PrimitiveType : public Type
  {
  public:
    PrimitiveType(TokenType type, size_t size);
    
    virtual std::unique_ptr<Type> Clone() const;
    
    TokenType GetType() const;
    
    // gets sizeof(type)
    size_t GetTypeSize() const;
    
    virtual bool IsEqual(Type *other) const;
  public:
    static PrimitiveType IntType;
    
  private:
    TokenType gType;
    size_t gSize;
  };
  
  class ArrayType : public Type
  {
  public:
    ArrayType(std::unique_ptr<Type> baseType, size_t count);
    
    virtual std::unique_ptr<Type> Clone() const;
  
    size_t GetTypeSize() const;
    
    const Type *GetBaseType() const;
    
    virtual bool IsEqual(Type *other) const;
    
    virtual bool IsReferenceType() const {
      return false;
    }
    
    virtual bool IsStackArray() const {
      return true;
    }
  private:
    std::unique_ptr<Type> gBaseType;
    size_t count;
  };
  
  class PointerType : public Type
  {
  public:
    PointerType(std::unique_ptr<Type> baseType);
    
    virtual std::unique_ptr<Type> Clone() const;
    
    size_t GetTypeSize() const;
    
    const Type *GetBaseType() const;
    
    virtual bool IsEqual(Type *other) const;
    
    virtual bool IsReferenceType() const {
      return true;
    }
    
  private:
    std::unique_ptr<Type> gBaseType;
  };
  
  class ParameterList : public Node
  {
  public:
    ParameterList();
    ParameterList(ParameterList &&other);
    
    void AppendParameter(std::unique_ptr<VariableInitializationStatement> parameter);
    void AppendParameters(std::unique_ptr<ParameterList> parameterList);
    
    std::vector<VariableInitializationStatement *> GetParameters() const;
    
    void Accept(Visitor *visitor);
    
  private:
    std::vector<std::unique_ptr<VariableInitializationStatement>> gParameters;
  };
  
  class Address : public Node
  {
  public:
    virtual ~Address() {}
    virtual void Accept(Visitor *visitor)=0;
    virtual bool IsImmediateOperand() const { return false; }
    virtual bool IsIdentifier() const { return false; }
    virtual bool IsEqual(Address *other) const = 0;
    
    /**
     * A simple address is an address that is either a plain ID or an immediate value.
     * An example of a non-simple address is one that either has a prefix operator, or
     * an id with an offset (e.g. i[0]). This is important because Complex expressions
     * must be made up of two simple expressions to keep with our three address code. Also
     * function parameters must be simple.
     */
    virtual bool IsSimpleAddress() const {
      return true;
    }
  };
  
  class IdentifierAddress : public Address
  {
  public:
    IdentifierAddress(std::string identifier);
    
    IdentifierAddress(std::string identifier, std::unique_ptr<Address> offset);
    
    IdentifierAddress(IdentifierAddress &&other);
    
    ~IdentifierAddress() {}
    
    std::string GetIdentifier() const;
    
    const Address *GetOffset() const;
    
    void Accept(Visitor* visitor);
    
    bool IsEqual(Address *other) const;
    
    /**
     * An Id address will be considered simple if it has no prefix or posfix ops.
     */
    virtual bool IsSimpleAddress() const;
    
    virtual bool IsIdentifier() const {
      return true;
    }
  private:
    std::unique_ptr<Address> gOffset;
    
    std::string gIdentifier;
  };
  
  class Int32Address : public Address
  {
  public:
    Int32Address(Int32 value);
    ~Int32Address() {}
    
    Int32 GetValue() const;
    
    size_t GetSize() const;
    
    bool IsImmediateOperand() const;
    
    void Accept(Visitor *visitor);
    
    bool IsEqual(Address *other) const;
  private:
    Int32 gValue;
  };
  
  class StatementList : public Node
  {
  public:
    StatementList();
    StatementList(StatementList &&other);
    void AppendStatement(std::unique_ptr<Statement> statement);
    void AppendStatements(std::unique_ptr<StatementList> statementList);
    std::vector<Statement *> GetStatements() const;
    void Accept(Visitor *visitor);
    
  private:
    std::vector<std::unique_ptr<Statement>> gStatements;
  };
  
  class Statement : public Node
  {
  public:
    virtual void Accept(Visitor *visitor)=0;
  };
  
  class AssignmentStatement : public Statement
  {
  public:
    AssignmentStatement(std::unique_ptr<IdentifierAddress> destination,
                        std::unique_ptr<Expression> expression,
                        TokenType prefixOperator=TokenType::None);
    
    AssignmentStatement(AssignmentStatement &&other);
    
    IdentifierAddress *GetDestination() const;
    
    Expression *GetExpression() const;
    
    TokenType GetDestinationPrefixOperator() const;
    
    void Accept(Visitor *visitor);
    
  private:
    TokenType gPrefixOperator;
    std::unique_ptr<IdentifierAddress> gDestination;
    std::unique_ptr<Expression> gExpression;
  };
  
  class VariableInitializationStatement : public Statement
  {
  public:
    VariableInitializationStatement(std::unique_ptr<Type> type, std::unique_ptr<IdentifierAddress> idAddr);
    VariableInitializationStatement(VariableInitializationStatement &&other);
    
    IdentifierAddress *GetIdentifierAddress() const;
    Type *GetType() const;
    void Accept(Visitor *visitor);
  private:
    std::unique_ptr<Type> gType;
    std::unique_ptr<IdentifierAddress> gIdentifierAddress;
  };
  
  class ReturnStatement : public Statement
  {
  public:
    ReturnStatement();
    ReturnStatement(std::unique_ptr<Address> returnAddress);
    ReturnStatement(ReturnStatement &&other);
    
    // optional address
    Address *GetReturnAddress() const;
    void SetReturnAddressValue(std::unique_ptr<Address> address);
    
    void Accept(Visitor *visitor);
  private:
    std::unique_ptr<Address> gReturnAddress;
  };
  
  class Expression : public Node
  {
  public:
    enum class ExpressionType
    {
      Complex,
      Simple,
      FunctionCall
    };
    
    Expression(ExpressionType type) : gType(type)
    {
    }
    
    Expression(Expression &&other)
    {
      gType = other.gType;
    }
    
    bool IsComplex() const
    {
      return gType == ExpressionType::Complex;
    }
    
    bool IsSimple() const
    {
      return gType == ExpressionType::Simple;
    }
    
    bool IsFunctionCall() const
    {
      return gType == ExpressionType::FunctionCall;
    }
  private:
    ExpressionType gType;
  };
  
  class CallStatement : public Statement, public Expression
  {
  public:
    CallStatement(std::unique_ptr<IdentifierAddress> functionId);
    CallStatement(CallStatement &&other);
    
    std::vector<Address *> GetArguments() const;
    
    void AppendArguments(std::unique_ptr<Address> argument);
    
    IdentifierAddress *GetFunctionId() const;
    
    void Accept(Visitor *visitor);
    
  private:
    std::vector<std::unique_ptr<Address>> gArguments;
    std::unique_ptr<IdentifierAddress> gFunctionId;
  };
  
  class IfStatement : public Statement
  {
  public:
    IfStatement(std::unique_ptr<Expression> expression,
                std::unique_ptr<LucyLabel> label);
    
    IfStatement(IfStatement &&other);
    
    LucyLabel *GetLabel() const;
    Expression *GetExpression() const;
    
    void Accept(Visitor *visitor);
    
  private:
    std::unique_ptr<Expression> gExpression;
    std::unique_ptr<LucyLabel> gLabel;
  };
  
  class GotoStatement : public Statement
  {
  public:
    GotoStatement(std::unique_ptr<LucyLabel> label);
    GotoStatement(GotoStatement &&other);
    
    const LucyLabel *GetLabel() const;
    
    void Accept(Visitor *visitor);
    
  private:
    std::unique_ptr<LucyLabel> gLabel;
  };
  
  class SimpleExpression : public Expression
  {
  public:
    SimpleExpression(std::unique_ptr<Address> address);
    SimpleExpression(std::unique_ptr<Address> address, TokenType prefixOperator);
    SimpleExpression(SimpleExpression &&other);
    
    Address *GetAddress() const;
    
    void Accept(Visitor *visitor);
    
    TokenType GetPrefixOperator() const;
    
  private:
    TokenType gPrefixOperator;
    std::unique_ptr<Address> gAddress;
  };
  
  class ComplexExpression : public Expression
  {
  public:
    ComplexExpression(std::unique_ptr<Address> left,
                      std::unique_ptr<Address> right,
                      TokenType                op);
    
    ComplexExpression(ComplexExpression &&other);
    
    Address *GetLHS() const;
    Address *GetRHS() const;
    TokenType                GetOp()  const;
    
    void Accept(Visitor *visitor);
  private:
    TokenType gOp;
    std::unique_ptr<Address> gLeftAddress;
    std::unique_ptr<Address> gRightAddress;
  };
  
  class LucyLabel : public Statement
  {
  public:
    LucyLabel(std::unique_ptr<IdentifierAddress> identifier);
    LucyLabel(LucyLabel &&other);
    IdentifierAddress *GetIdentifer() const;
    
    void Accept(Visitor *visitor);
  private:
    std::unique_ptr<IdentifierAddress> gLabelIdentifier;
    
  };
}
