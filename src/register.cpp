#include "register.h"

#include <cassert>

/*=================================
 * Register
 *=================================
 */

using namespace lcy;
using namespace x64;

Register::Register(std::string quadWordReg, std::string doublWordReg, std::string wordReg, std::string byteReg, OpSize size)
{
  gQuadWordRegisterName = quadWordReg;
  gDoubleWordRegisterName = doublWordReg;
  gWordRegisterName = wordReg;
  gByteRegisterName = byteReg;
  gRegisterSize = size;
}

std::string Register::ToString() const
{
  return "%" + GetName();
}

std::string Register::GetName() const
{
  if (GetSize() == OpSizeQuadWord) {
    return gQuadWordRegisterName;
  } else if (GetSize() == OpSizeDoubleWord) {
    return gDoubleWordRegisterName;
  } else if (GetSize() == OpSizeWord) {
    return gWordRegisterName;
  } else {
    return gByteRegisterName;
  }
}

size_t Register::GetSize() const
{
  return (size_t)gRegisterSize;
}

std::shared_ptr<Register> Register::RegisterForSize(OpSize size) const
{
  switch (size) {
    case OpSizeByte:
      return GetByteRegister();
    case OpSizeWord:
      return GetWordRegister();
    case OpSizeDoubleWord:
      return GetDoubleWordRegister();
    case OpSizeQuadWord:
      return GetQuadWordRegister();
    default:
      return nullptr;
  }
}

std::shared_ptr<Register> Register::GetShared() const
{
  return std::make_shared<Register>(Register(gQuadWordRegisterName,
                                             gDoubleWordRegisterName,
                                             gWordRegisterName,
                                             gByteRegisterName, (OpSize)GetSize()));
}

std::shared_ptr<Register> Register::GetQuadWordRegister() const
{
    return Register(gQuadWordRegisterName,
                    gDoubleWordRegisterName,
                    gWordRegisterName,
                    gByteRegisterName, OpSizeQuadWord).GetShared();
}

std::shared_ptr<Register> Register::GetDoubleWordRegister() const
{
  return Register(gQuadWordRegisterName,
                  gDoubleWordRegisterName,
                  gWordRegisterName,
                  gByteRegisterName, OpSizeDoubleWord).GetShared();
}

std::shared_ptr<Register> Register::GetWordRegister() const
{
  return Register(gQuadWordRegisterName,
                  gDoubleWordRegisterName,
                  gWordRegisterName,
                  gByteRegisterName, OpSizeWord).GetShared();
}

std::shared_ptr<Register> Register::GetByteRegister() const
{
  return Register(gQuadWordRegisterName,
                  gDoubleWordRegisterName,
                  gWordRegisterName,
                  gByteRegisterName, OpSizeByte).GetShared();
}

bool Register::IsEqual(const std::shared_ptr<Register> other) const
{
  return (GetName() == other->GetName() &&
          gRegisterSize == other->GetSize());
}

// 64 Bit Registers
Register Register::Rax = Register("rax", "eax", "ax", "al", OpSizeQuadWord);
Register Register::Rcx = Register("rcx", "ecx", "cx", "cl", OpSizeQuadWord);
Register Register::Rdx = Register("rdx", "edx", "dx", "dl", OpSizeQuadWord);
Register Register::Rbx = Register("rbx", "ebx", "bx", "bl", OpSizeQuadWord);
Register Register::Rsi = Register("rsi", "esi", "si", "sil", OpSizeQuadWord);
Register Register::Rdi = Register("rdi", "edi", "di", "dil", OpSizeQuadWord);
Register Register::Rsp = Register("rsp", "esp", "sp", "spl", OpSizeQuadWord);
Register Register::Rbp = Register("rbp", "ebp", "bp", "bpl", OpSizeQuadWord);
Register Register::R8 = Register("r8", "r8d", "r8w", "r8b", OpSizeQuadWord);
Register Register::R9 = Register("r9","r9d", "r9w", "r9b", OpSizeQuadWord);

/*=================================
 * Immediate
 *=================================
 */
ImmediateOperand::ImmediateOperand(Int64 value, size_t size) : gValue(value)
{
  gSize = size;
}

Int64 ImmediateOperand::GetValue() const
{
  return gValue;
}

size_t ImmediateOperand::GetSize() const
{
  return gSize;
}

std::string ImmediateOperand::ToString() const
{
  return "$" + std::to_string(gValue);
}

/*=================================
 * Offset
 *=================================
 */

BaseRelativeOperand::BaseRelativeOperand(std::shared_ptr<Register> reg, int offset, size_t size) : gOffset(offset), gRegister(reg)
{
  gSize = size;
}

std::string BaseRelativeOperand::ToString() const
{
  std::string offsetStr = std::to_string(gOffset);
  
  return offsetStr + "(" + gRegister->ToString() + ")";
}

int BaseRelativeOperand::GetOffset() const
{
  return gOffset;
}

size_t BaseRelativeOperand::GetSize() const
{
  return gSize;
}

std::shared_ptr<Register> BaseRelativeOperand::GetRegister() const
{
  return gRegister;
}

/*=================================
 * Array Access
 *=================================
 */

ArrayAccessOperand::ArrayAccessOperand(std::shared_ptr<Register> arrayAddress, std::shared_ptr<Register> offsetReg, size_t size)
: gArrayAddress(arrayAddress),
  gOffsetRegister(offsetReg),
  gElementSize(size)
{
}

std::string ArrayAccessOperand::ToString() const
{
  return "(" + gArrayAddress->ToString() + ", " + gOffsetRegister->ToString() + ", " + std::to_string(gElementSize) + ")";
}

std::shared_ptr<Register> ArrayAccessOperand::GetOffsetRegister() const
{
  return gOffsetRegister;
}

std::shared_ptr<Register> ArrayAccessOperand::GetArrayAddressRegister() const
{
  return gArrayAddress;
}

size_t ArrayAccessOperand::GetSize() const
{
  return gElementSize;
}
