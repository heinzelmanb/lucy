#include "compiler.h"

#include "parser.h"
#include "typechecker.h"
#include "lexer.h"
#include "codegenerator.h"
#include "filesource.h"
#include "exceptions.h"

#include <fstream>
#include <memory>
#include <iostream>

using namespace lcy;

Compiler::Compiler(std::shared_ptr<CompilerOptions> options)
: gOptions(options)
{
}

static void writeCodeToFile(const std::string &path, Assembly *assembly)
{
  std::ofstream output(path);
  if (output.is_open()) {
    output << assembly->ToString() << std::endl;
  }
  output.close();
}

bool Compiler::Compile(std::string &filename)
{
  std::ifstream input(filename, std::ios::binary);
  
  if (input.is_open() == false) {
    std::cerr << "Error: Could not open file" << std::endl;
    return false;
  }
  
  lcy::FileSource fileSource(&input);
  
  lcy::Lexer lex(std::make_shared<lcy::FileSource>(fileSource));
  lcy::Parser parser(std::make_shared<lcy::Lexer>(lex));
  
  try {
    std::unique_ptr<lcy::Program> ast = parser.Parse();
    auto typeChecker = lcy::TypeChecker();
    typeChecker.CheckType(ast.get());
    
    lcy::CodeGenerator codeGenerator = lcy::CodeGenerator();
    
    std::shared_ptr<lcy::Assembly> code = codeGenerator.GenerateCode(ast.get());
    
    if (gOptions->outputToStdOut) {
      std::cout << code->ToString() << std::endl;
    }
  
    if (gOptions->writeToFile) {
      writeCodeToFile(gOptions->outputFile, code.get());
    }
    
  } catch (lcy::ParseError parseError) {
    std::cerr << parseError.what() << std::endl;
    return false;
  } catch (lcy::TypeError typeError) {
    std::cerr << typeError.what() << std::endl;
    return false;
  } catch (...) {
    std::cerr << "Fatal Error; Unknown error occurred" << std::endl;
    return false;
  }
  return true;
}
