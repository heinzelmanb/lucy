#pragma once

#include <memory>

/**
 * Interface for this visitor pattern.
 */

namespace lcy
{
  //forward declarations
  class Program;
  class FunctionDeclaration;
  class StatementList;
  class IdentifierAddress;
  class Int32Address;
  class AssignmentStatement;
  class ReturnStatement;
  class CallStatement;
  class VariableInitializationStatement;
  class ParameterList;
  class SimpleExpression;
  class ComplexExpression;
  class LucyLabel;
  class IfStatement;
  class GotoStatement;
  
  class Visitor
  {
  public:
    virtual void Visit(Program                         *program)=0;
    virtual void Visit(FunctionDeclaration             *functionDecl)=0;
    virtual void Visit(StatementList                   *statementList)=0;
    virtual void Visit(IdentifierAddress               *address)=0;
    virtual void Visit(Int32Address                    *address)=0;
    virtual void Visit(AssignmentStatement             *statement)=0;
    virtual void Visit(ReturnStatement                 *statement)=0;
    virtual void Visit(CallStatement                   *statement)=0;
    virtual void Visit(VariableInitializationStatement *statement)=0;
    virtual void Visit(IfStatement                     *statement)=0;
    virtual void Visit(GotoStatement                   *statement)=0;
    virtual void Visit(ParameterList                   *parameterList)=0;
    virtual void Visit(SimpleExpression                *simpleExpression)=0;
    virtual void Visit(ComplexExpression               *complexExpression)=0;
    virtual void Visit(LucyLabel                       *label)=0;
  };
  
}
