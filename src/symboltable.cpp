#include "symboltable.h"

using namespace lcy;

void SymbolTableFrame::AddDataForId(const std::string id, std::shared_ptr<VariableData> data)
{
  gCache[id] = data;
}

VariableData *SymbolTableFrame::GetDataForId(const std::string id) const
{
  if (gCache.count(id)) {
    auto &entry = gCache.at(id);
    return entry.get();
  }
  return nullptr;
}

bool SymbolTableFrame::IdExists(const std::string id) const
{
  return gCache.count(id);
}

void SymbolTableFrame::Clear()
{
  gCache.clear();
}

SymbolTable::SymbolTable()
{
}

SymbolTableFrame *SymbolTable::TopFrame()
{
  auto &top = gFrames.top();
  return top.get();
}

void SymbolTable::PopFrame()
{
  gFrames.pop();
}

void SymbolTable::PushFrame()
{
  gFrames.push(std::make_shared<SymbolTableFrame>());
}
