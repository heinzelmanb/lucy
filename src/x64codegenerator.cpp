#ifdef __x86_64

#include "x64codegenerator.h"
#include "ast.h"
#include "x64instructions.h"
#include "x64assembly.h"

#include "debug.h"

using namespace lcy;
using namespace x64;

static void initialize32bitArgumentRegisters(std::vector<std::shared_ptr<Register>> &registers)
{
  registers.push_back(Register::Rdi.GetShared()->GetDoubleWordRegister());
  registers.push_back(Register::Rsi.GetShared()->GetDoubleWordRegister());
  registers.push_back(Register::Rdx.GetShared()->GetDoubleWordRegister());
  registers.push_back(Register::Rcx.GetShared()->GetDoubleWordRegister());
  registers.push_back(Register::R8.GetShared()->GetDoubleWordRegister());
  registers.push_back(Register::R9.GetShared()->GetDoubleWordRegister());
}

static void initializeAvailable32BitRegisters(std::vector<std::shared_ptr<Register>> &registers) {
  registers.push_back(Register::Rax.GetShared()->GetDoubleWordRegister());
  registers.push_back(Register::Rbx.GetShared()->GetDoubleWordRegister());
  registers.push_back(Register::Rcx.GetShared()->GetDoubleWordRegister());
  registers.push_back(Register::Rdx.GetShared()->GetDoubleWordRegister());
  registers.push_back(Register::Rdi.GetShared()->GetDoubleWordRegister());
  registers.push_back(Register::Rsi.GetShared()->GetDoubleWordRegister());
  registers.push_back(Register::Rdx.GetShared()->GetDoubleWordRegister());
  registers.push_back(Register::Rcx.GetShared()->GetDoubleWordRegister());
  registers.push_back(Register::R8.GetShared()->GetDoubleWordRegister());
  registers.push_back(Register::R9.GetShared()->GetDoubleWordRegister());
}

x64CodeGenerator::x64CodeGenerator()
{
  initializeAvailable32BitRegisters(gAvailable32BitRegisters);
  initialize32bitArgumentRegisters(gArgument32BitRegisters);
}

x64CodeGenerator::~x64CodeGenerator()
{
}

std::shared_ptr<Assembly> x64CodeGenerator::GenerateCode(Program *program)
{
  gAssembly = std::make_shared<X64Assembly>();
  program->Accept(this);
  return gAssembly;
}

/*---------------------------
 * Visitor implementation
 *---------------------------
 */
void x64CodeGenerator::Visit(Program *program)
{
  for (auto decl : program->GetDeclarationList()) {
    decl->Accept(this);
  }
}

void x64CodeGenerator::Visit(FunctionDeclaration *functionDecl)
{
  gScopes.PushFrame();
  gStackOffset.push(0);
  
  std::string functionName = functionDecl->GetFunctionIdentifier()->GetIdentifier();
  gCurrentFunctionId = functionName;
  
  generateFunctionDirectives(functionDecl);
 
  auto pushEbp       =  std::make_shared<PushInstruction>(OpSizeQuadWord,
                                                          Register::Rbp.GetShared());
  auto movSpToBp     =  std::make_shared<MoveInstruction>(OpSizeQuadWord,
                                                          Register::Rsp.GetShared(),
                                                          Register::Rbp.GetShared());
  gAssembly->AddInstruction(pushEbp);
  gAssembly->AddInstruction(movSpToBp);
  
  if (functionDecl->GetParameterList()) {
    int i = 0;
    auto parameters = functionDecl->GetParameterList()->GetParameters();
    for (auto it = parameters.rbegin(); it != parameters.rend(); ++it) {
      auto param = *it;
      // make space for var
      param->Accept(this);
      
      OpSize parameterSize = (OpSize)param->GetType()->GetTypeSize();
      
      auto parameterRegister = gArgument32BitRegisters[i++]->RegisterForSize(parameterSize);
      std::shared_ptr<BaseRelativeOperand> var = getBaseRelativeOperand(param->GetIdentifierAddress());
      std::shared_ptr<MoveInstruction> mov = std::make_shared<MoveInstruction>(parameterSize, parameterRegister, var);
      gAssembly->AddInstruction(mov);
    }
  }
  
  functionDecl->GetStatementList()->Accept(this);
  
  // add cleanup label
  gAssembly->AddLabel(generateFunctionExitLabel());
  
  // clear up local variables
  if (gStackOffset.top() > 0) {
    std::shared_ptr<ImmediateOperand> stackSize = std::make_shared<ImmediateOperand>(gStackOffset.top(), (size_t)OpSizeQuadWord);
    gAssembly->AddInstruction(std::make_shared<AddInstruction>(OpSizeQuadWord, stackSize, Register::Rsp.GetShared()));
  }
  
  auto popRbp = std::make_shared<PopInstruction>(OpSizeQuadWord, Register::Rbp.GetShared());
  gAssembly->AddInstruction(popRbp);
  generateReturnInstruction();
  
  gScopes.PopFrame();
  gStackOffset.pop();
}

void x64CodeGenerator::Visit(StatementList *statementList)
{
  std::vector<VariableInitializationStatement *> initializationStmts;
  std::vector<Statement *> statements;
  
  for (auto statement : statementList->GetStatements()) {
    auto ptr = dynamic_cast<VariableInitializationStatement *>(statement);
    if (ptr != nullptr) {
      initializationStmts.push_back(ptr);
    } else {
      statements.push_back(statement);
    }
  }
  // make all of the variable initializations before the other statements so that stack cleanup is easier.
  for (auto varInitStatement : initializationStmts) {
    varInitStatement->Accept(this);
  }
  
  for (auto statement : statements) {
    statement->Accept(this);
  }
}

void x64CodeGenerator::Visit(IdentifierAddress *address)
{
}

void x64CodeGenerator::Visit(Int32Address *address)
{
}

void x64CodeGenerator::Visit(AssignmentStatement *statement)
{
  Expression *expression = statement->GetExpression();
  if (expression->IsComplex()) {
    ComplexExpression *complex = static_cast<ComplexExpression *>(statement->GetExpression());
    assign(statement->GetDestination(), complex);
  } else if (expression->IsSimple()) {
    SimpleExpression *simple = static_cast<SimpleExpression *>(statement->GetExpression());
    assign(statement->GetDestination(), simple, statement->GetDestinationPrefixOperator());
  } else {
    CallStatement *call = static_cast<CallStatement *>(statement->GetExpression());
    call->Accept(this);
    auto destination = getBaseRelativeOperand(statement->GetDestination());
    // TODO check size of return and fill correct register
    auto move = std::make_shared<MoveInstruction>((OpSize)destination->GetSize(), Register::Rax.GetShared()->GetDoubleWordRegister(), destination);
    gAssembly->AddInstruction(move);
  }
}

void x64CodeGenerator::Visit(VariableInitializationStatement *statement)
{
  size_t variableSize = statement->GetType()->GetTypeSize();
  std::shared_ptr<ImmediateOperand> offset = std::make_shared<ImmediateOperand>(variableSize, variableSize);
  std::shared_ptr<Register> sp = Register::Rsp.GetShared();
  SubtractInstruction subtractSp = SubtractInstruction(OpSizeQuadWord, offset, sp);
  gAssembly->AddInstruction(std::make_shared<SubtractInstruction>(subtractSp));
  
  // increase the current offset
  addToCurrentOffset(variableSize);
  // store the variable name and offset in the scopes map
  std::shared_ptr<VariableData> data = std::make_shared<VariableData>(-gStackOffset.top(), statement->GetType()->GetTypeSize(), statement->GetType());
  gScopes.TopFrame()->AddDataForId(statement->GetIdentifierAddress()->GetIdentifier(), data);
}

void x64CodeGenerator::Visit(ReturnStatement *statement)
{
  // TODO check for 64 bit.
  std::shared_ptr<Register> eax = Register::Rax.GetShared()->GetDoubleWordRegister();
  Address *returnAddress = statement->GetReturnAddress();
  if (returnAddress != nullptr) {
    loadRegister(returnAddress, eax);
  }
  gAssembly->AddJmp(std::make_shared<JmpInstruction>(generateFunctionExitLabel()));
}

void x64CodeGenerator::Visit(CallStatement *callStatement)
{
  // calling conventions
  // https://aaronbloomfield.github.io/pdr/book/x86-64bit-ccc-chapter.pdf
  auto args = callStatement->GetArguments();
  
  assert(args.size() <= 6);
  
  std::vector<std::shared_ptr<Register>> registersToPop;
  
  int i = 0;
  for (auto it = args.rbegin(); it != args.rend(); ++it, i++) {
    std::shared_ptr<Register> reg = gArgument32BitRegisters[i];
    Address *address = *it;
    
    // check if its in use.
    if (isRegisterInUse(reg) == true) {
      // push parent 64 bit register to stack      
      push(reg->GetQuadWordRegister());
      registersToPop.push_back(reg->GetQuadWordRegister());
    }
    
    OpSize size = OpSizeNone;
    if (address->IsIdentifier()) {
      IdentifierAddress *id = static_cast<IdentifierAddress *>(address);
      VariableData *data = gScopes.TopFrame()->GetDataForId(id->GetIdentifier());
      size = (OpSize)data->GetSize();
    } else {
      size = (OpSize)(static_cast<Int32Address *>(address)->GetSize());
    }
    
    reg = reg->RegisterForSize(size);
    
    loadRegister(address, reg);
  }
  
  // make sure stack offset is a multiple of 16 or else we get a seg fault
  // https://stackoverflow.com/questions/43354658/os-x-x64-stack-not-16-byte-aligned-error
  int currentStackOffset = gStackOffset.top() % 16;
  if (currentStackOffset != 0) {
    std::shared_ptr<ImmediateOperand> imm = std::make_shared<ImmediateOperand>(currentStackOffset, OpSizeQuadWord);
    std::shared_ptr<SubtractInstruction> sub = std::make_shared<SubtractInstruction>(OpSizeQuadWord, imm, Register::Rsp.GetShared());
    gAssembly->AddInstruction(sub);
  }
  
  // call function
  #if __APPLE__
    std::string functionName = "_" + callStatement->GetFunctionId()->GetIdentifier();
  #else
    std::string functionName = callStatement->GetFunctionId()->GetIdentifier();
  #endif
  
  std::shared_ptr<Label> functionLabel = std::make_shared<Label>(functionName);
  std::shared_ptr<CallInstruction> call = std::make_shared<CallInstruction>(functionLabel);
  gAssembly->AddInstruction(call);
  
  if (currentStackOffset != 0) {
    std::shared_ptr<ImmediateOperand> imm = std::make_shared<ImmediateOperand>(currentStackOffset, OpSizeQuadWord);
    std::shared_ptr<AddInstruction> add = std::make_shared<AddInstruction>(OpSizeQuadWord, imm, Register::Rsp.GetShared());
    gAssembly->AddInstruction(add);
  }
  
  // pop registers that were pushed
  for (auto reg : registersToPop) {
    pop(reg);
  }
}

void x64CodeGenerator::Visit(IfStatement *statement)
{
  Expression *exp = statement->GetExpression();
  std::shared_ptr<Register> leftOperand = nullptr;
  std::shared_ptr<Register> rightOperand = nullptr;
  
  Address *leftAddress;
  Address *rightAddress;
  
  TokenType op;
  
  bool freeLeftAddr = false;
  
  if (exp->IsSimple()) {
    SimpleExpression *simp = static_cast<SimpleExpression *>(statement->GetExpression());

    rightAddress = simp->GetAddress();
    leftAddress = new Int32Address(1);
    freeLeftAddr = true;
    op = TokenType::Equals;
  } else {
   ComplexExpression *complex = static_cast<ComplexExpression *>(statement->GetExpression());
    rightAddress = complex->GetRHS();
    leftAddress = complex->GetLHS();
    op = complex->GetOp();
  }
  
  leftOperand = loadRegister(leftAddress);
  
  rightOperand = loadRegister(rightAddress);
  
  if (Token(op, 0).IsRelationalOperator()) {
    evaluateRelationalExpression(leftOperand, rightOperand, op, createLabelFromLucyLabel(statement->GetLabel()));
  } else {
    evaluateExpression(leftOperand, rightOperand, op, createLabelFromLucyLabel(statement->GetLabel()));
  }
  
  release32BitRegister(leftOperand);
  release32BitRegister(rightOperand);
  
  if (freeLeftAddr) {
    delete leftAddress;
  }
}

void x64CodeGenerator::Visit(LucyLabel *label)
{
  gAssembly->AddLabel(createLabelFromLucyLabel(label));
}

void x64CodeGenerator::Visit(GotoStatement *statement)
{
  std::shared_ptr<Label> label = createLabelFromLucyLabel((LucyLabel *)statement->GetLabel());
  std::shared_ptr<JmpInstruction> jump = std::make_shared<JmpInstruction>(label);
  gAssembly->AddInstruction(jump);
}

// Not implemented visitors
void x64CodeGenerator::Visit(ParameterList *parameterList)
{
}

void x64CodeGenerator::Visit(SimpleExpression *simpleExpression)
{
}

void x64CodeGenerator::Visit(ComplexExpression *complexExpression)
{
}

/*---------------------------
 * Helpers implementation
 *---------------------------
 */
void x64CodeGenerator::assign(IdentifierAddress *destinationAddress, SimpleExpression *expression, TokenType prefixOperator)
{
  std::vector<std::shared_ptr<Register>> regsToFree;
  
  std::shared_ptr<Operand> destinationOperand = nullptr;
  std::shared_ptr<Operand> sourceOperand = nullptr;
  
  Address *source = expression->GetAddress();
  
  bool destShouldBeInRegister = false;
  
  if (dynamic_cast<Int32Address *>(source) != nullptr) {
    auto intAddress = dynamic_cast<Int32Address *>(source);
    sourceOperand = evalPrefixOperator(intAddress, expression->GetPrefixOperator());
  } else if (dynamic_cast<IdentifierAddress *>(source) != nullptr) {
    IdentifierAddress *id = static_cast<IdentifierAddress *>(source);
    
    if (id->GetOffset() == nullptr) {
      sourceOperand = evalPrefixOperator(id, expression->GetPrefixOperator(), regsToFree);
      if (!sourceOperand) {
        auto sourceReg = loadRegister(id);
        regsToFree.push_back(sourceReg);
        sourceOperand = sourceReg;
      }
    } else {
      // bad need to use LEAQ
      std::shared_ptr<ArrayAccessOperand> arrayAccess = getArrayAccessOperand(id);
      regsToFree.push_back(arrayAccess->GetArrayAddressRegister());
      regsToFree.push_back(arrayAccess->GetOffsetRegister());
      sourceOperand = arrayAccess;
      destShouldBeInRegister = true;
      
      // make sure a prefix op is not used with an offset
      assert(expression->GetPrefixOperator() == TokenType::None);
    }
  }
  
  if (destinationAddress->GetOffset() == nullptr && prefixOperator == TokenType::None) {
    if (destShouldBeInRegister == false) {
      destinationOperand = getBaseRelativeOperand(destinationAddress);
    } else {
      std::shared_ptr<Register> destinationReg = loadRegister(destinationAddress);
      destinationOperand = destinationReg;
      regsToFree.push_back(destinationReg);
    }
  } else if (prefixOperator != TokenType::None) {
    switch (prefixOperator) {
      case TokenType::Multiply: {
        std::shared_ptr<Register> destinationReg = loadRegister(destinationAddress, getAvailable32BitRegister()->GetQuadWordRegister());
        std::shared_ptr<BaseRelativeOperand> dereference = std::make_shared<BaseRelativeOperand>(destinationReg, 0, OpSizeQuadWord);
        regsToFree.push_back(destinationReg);
        destinationOperand = dereference;
        break;
      }
      default:
        FAIL("Invalid left hand prefix operator");
        break;
    }
  } else {
    // bad need to use LEAQ
    std::shared_ptr<ArrayAccessOperand> arrayAccess = getArrayAccessOperand(destinationAddress);
    regsToFree.push_back(arrayAccess->GetOffsetRegister());
    regsToFree.push_back(arrayAccess->GetArrayAddressRegister());
    destinationOperand = arrayAccess;
  }
  gAssembly->AddInstruction(std::make_shared<MoveInstruction>((OpSize)sourceOperand->GetSize(), sourceOperand, destinationOperand));
  
  if (destShouldBeInRegister) {
    // as of now destination will only be in a register is if the source is an array access.
    std::shared_ptr<BaseRelativeOperand> variableOffsetOp = getBaseRelativeOperand(destinationAddress);
    gAssembly->AddInstruction(std::make_shared<MoveInstruction>((OpSize)destinationOperand->GetSize(), destinationOperand, variableOffsetOp));
  }
  
  for (auto& reg : regsToFree) {
    release32BitRegister(reg);
  }
}

void x64CodeGenerator::assign(IdentifierAddress *destination, ComplexExpression *expression)
{
  std::vector<std::shared_ptr<Register>> registersInUse;
  std::vector<Address *> remaining;
  std::shared_ptr<Operand> sourceOperand = nullptr;
  std::shared_ptr<Operand> destinationOperand = nullptr;
  
  bool needFinalMoveInstruction = true;
  
  assert(expression != nullptr);
  assert(expression->GetRHS() != nullptr);
  assert(expression->GetLHS() != nullptr);
  
  if (Token(expression->GetOp(), 0).IsRelationalOperator()) {
    std::shared_ptr<Register> leftReg = loadRegister(expression->GetLHS());
    std::shared_ptr<Register> rightReg = loadRegister(expression->GetRHS());
    
    evaluateRelationalExpression(leftReg, rightReg, expression->GetOp());
    
    destinationOperand = rightReg;
    sourceOperand = leftReg;
    
    registersInUse.push_back(leftReg);
    registersInUse.push_back(rightReg);
  } else {
    bool forceDestinationInRegister = false;
    
    if (expression->GetOp() == TokenType::Multiply) {
      forceDestinationInRegister = true;
    }
    
    if (expression->GetLHS()->IsImmediateOperand()) {
      // LHS immedate
      auto integer = static_cast<Int32Address *>(expression->GetLHS());
      sourceOperand = std::make_shared<ImmediateOperand>(integer->GetValue(), integer->GetSize());
      remaining.push_back(expression->GetRHS());
    } else if (expression->GetRHS()->IsImmediateOperand()) {
      auto integer = static_cast<Int32Address *>(expression->GetRHS());
      sourceOperand = std::make_shared<ImmediateOperand>(integer->GetValue(), integer->GetSize());
      remaining.push_back(expression->GetLHS());
    } else if (expression->GetLHS()->IsImmediateOperand() == false &&
               expression->GetRHS()->IsImmediateOperand() == false) {
      remaining.push_back(expression->GetLHS());
      remaining.push_back(expression->GetRHS());
    }
    
    for (auto address : remaining) {
      if (address->IsEqual(destination) && destinationOperand == nullptr && !forceDestinationInRegister) {
        destinationOperand = getBaseRelativeOperand(static_cast<IdentifierAddress *>(address));
        needFinalMoveInstruction = false;
      } else {
        registersInUse.push_back(loadRegister(address));
      }
    }
    
    int registerIdx = 0;
    if (sourceOperand == nullptr) {
      assert(registerIdx < registersInUse.size());
      sourceOperand = registersInUse[registerIdx++];
    }
    
    if (destinationOperand == nullptr) {
      assert(registerIdx < registersInUse.size());
      destinationOperand = registersInUse[registerIdx];
    }
    
    evaluateExpression(sourceOperand,
                       destinationOperand,
                       expression->GetOp());
  }
  
  
  if (needFinalMoveInstruction) {
    // Now make the assignment
    std::shared_ptr<BaseRelativeOperand> finalDestination = getBaseRelativeOperand(destination);
    
    gAssembly->AddInstruction(std::make_shared<MoveInstruction>((OpSize)destinationOperand->GetSize(), destinationOperand, finalDestination));
  }
  
  for (auto reg : registersInUse) {
    release32BitRegister(reg);
  }
}

void x64CodeGenerator::evaluateRelationalExpression(std::shared_ptr<Register> source, std::shared_ptr<Register> destination, TokenType op, std::shared_ptr<Label> jmpLabel)
{
  assert(Token(op, 0).IsRelationalOperator());
  OpSize size = (OpSize)destination->GetSize();
  
  switch (op) {
    case TokenType::Equals:
      gAssembly->AddInstruction(std::make_shared<CmpInstruction>(size, source, destination));
      if (jmpLabel) {
        gAssembly->AddInstruction(std::make_shared<JmpEInstruction>(jmpLabel));
      } else {
        gAssembly->AddInstruction(std::make_shared<SetEInstruction>(destination->GetByteRegister()));
      }
      break;
    case TokenType::NotEqual:
      gAssembly->AddInstruction(std::make_shared<CmpInstruction>(size, source, destination));
      if (jmpLabel) {
        gAssembly->AddInstruction(std::make_shared<JmpNEInstruction>(jmpLabel));
      } else {
        gAssembly->AddInstruction(std::make_shared<SetNEInstruction>(destination->GetByteRegister()));
      }
      break;
    case TokenType::GreaterThan:
      gAssembly->AddInstruction(std::make_shared<CmpInstruction>(size, destination, source));
      if (jmpLabel) {
        gAssembly->AddInstruction(std::make_shared<JmpGInstruction>(jmpLabel));
      } else {
        gAssembly->AddInstruction(std::make_shared<SetGInstruction>(destination->GetByteRegister()));
      }
      break;
    case TokenType::LessThan:
      gAssembly->AddInstruction(std::make_shared<CmpInstruction>(size, destination, source));
      if (jmpLabel) {
        gAssembly->AddInstruction(std::make_shared<JmpLInstruction>(jmpLabel));
      } else {
        gAssembly->AddInstruction(std::make_shared<SetLInstruction>(destination->GetByteRegister()));
      }
      break;
    case TokenType::GreaterThanEqual:
      gAssembly->AddInstruction(std::make_shared<CmpInstruction>(size, destination, source));
      if (jmpLabel) {
        gAssembly->AddInstruction(std::make_shared<JmpGEInstruction>(jmpLabel));
      } else {
        gAssembly->AddInstruction(std::make_shared<SetGEInstruction>(destination->GetByteRegister()));
      }
      break;
    case TokenType::LessThanEqual:
      gAssembly->AddInstruction(std::make_shared<CmpInstruction>(size, destination, source));
      if (jmpLabel) {
        gAssembly->AddInstruction(std::make_shared<JmpLEInstruction>(jmpLabel));
      } else {
        gAssembly->AddInstruction(std::make_shared<SetLEInstruction>(destination->GetByteRegister()));
      }
      break;
    default:
      std::cerr << "Codegen error: unknown relational operator" << std::endl;
      assert(0);
  }
}

void x64CodeGenerator::evaluateExpression(std::shared_ptr<Operand> source, std::shared_ptr<Operand> destination, TokenType op, std::shared_ptr<Label> jumpLabel)
{
  assert(Token(op, 0).IsRelationalOperator() == false);
  assert(source->GetSize() <= destination->GetSize());
  
  OpSize size = (OpSize)destination->GetSize();
  bool madeJump = false;
  
  switch (op) {
    case TokenType::Add:
       gAssembly->AddInstruction(std::make_shared<AddInstruction>(size, source, destination));
      break;
    case TokenType::Subtract:
      gAssembly->AddInstruction(std::make_shared<SubtractInstruction>(size, source, destination));
      break;
    case TokenType::Multiply:
      if (size == OpSizeDoubleWord) {
        gAssembly->AddInstruction(std::make_shared<IMulInstruction>(size, source, destination));
      } else {
        // I am not sure if multiplication will work the same way for 64 bit. 
        TODO();
      }
      break;
    case TokenType::Divide:
      TODO();
    default:
      break;
  }
  
  if (jumpLabel && madeJump == false) {
    Int32Address zeroValue = Int32Address(0);
    std::shared_ptr<Register> zeroRegister = loadRegister(&zeroValue);
    std::shared_ptr<Register> destinationReg = nullptr;
    if (destination->IsRegister() == false) {
      TODO();
    } else {
      destinationReg = std::static_pointer_cast<Register>(destination);
    }
    
    // cmp result to one and make branch
    gAssembly->AddInstruction(std::make_shared<CmpInstruction>(size, zeroRegister, destinationReg));
    gAssembly->AddInstruction(std::make_shared<JmpGInstruction>(jumpLabel));
  }
}

std::shared_ptr<Operand> x64CodeGenerator::evalPrefixOperator(IdentifierAddress *id, TokenType prefixOperator, std::vector<std::shared_ptr<Register>> &resgistersToFree)
{
  switch (prefixOperator) {
    case TokenType::Subtract: {
      std::shared_ptr<Register> reg = loadRegister(id);
      gAssembly->AddInstruction(std::make_shared<NegInstruction>((OpSize)reg->GetSize(), reg));
      resgistersToFree.push_back(reg);
      return reg;
    }
    case TokenType::BitwiseAnd: {
      return loadEffectiveAddress(id);
    }
    case TokenType::Multiply: {
      VariableData *data = gScopes.TopFrame()->GetDataForId(id->GetIdentifier());
      std::shared_ptr<Register> reg = loadRegister(id)->GetQuadWordRegister();
      resgistersToFree.push_back(reg);
      std::shared_ptr<BaseRelativeOperand> ptrDereference = std::make_shared<BaseRelativeOperand>(reg, 0, data->GetSize());
      
      std::shared_ptr<Register> reg2 = getAvailable32BitRegister()->GetQuadWordRegister();
      resgistersToFree.push_back(reg2);
      gAssembly->AddInstruction(std::make_shared<MoveInstruction>(OpSizeQuadWord, ptrDereference, reg2));
      
      PointerType *type = static_cast<PointerType *>((Type *)data->GetType());
      OpSize baseSize = (OpSize)type->GetBaseType()->GetTypeSize();
      return reg2->RegisterForSize(baseSize);
    }
    default:
      break;
  }
  return nullptr;
}

std::shared_ptr<ImmediateOperand> x64CodeGenerator::evalPrefixOperator(Int32Address *address, TokenType prefixOperator)
{
  OpSize size = (OpSize)address->GetSize();
  Int32 value = address->GetValue();
  
  switch (prefixOperator) {
    case TokenType::Subtract:
      return std::make_shared<ImmediateOperand>(-value, size);
    case TokenType::None:
      return std::make_shared<ImmediateOperand>(value, size);
    default:
      FAIL("Invalid prefix operator");
      return nullptr;
  }
}

std::shared_ptr<Label> x64CodeGenerator::generateFunctionExitLabel()
{
  return std::make_shared<Label>("_" + gCurrentFunctionId + "_end");
}

void x64CodeGenerator::generateFunctionDirectives(FunctionDeclaration *functionDecl)
{
  std::string functionName = functionDecl->GetFunctionIdentifier()->GetIdentifier();
  
  #if __APPLE__
    functionName = "_" + functionName;
  #endif
  
  std::shared_ptr<Directive> directive = std::make_shared<Directive>("globl " + functionName);
  auto functionLabel =  std::make_shared<Label>(functionName);
  gAssembly->AddDirective(directive);
  gAssembly->AddLabel(functionLabel);
}

void x64CodeGenerator::generateReturnInstruction()
{
  gAssembly->AddInstruction(std::make_shared<ReturnInstruction>());
}

/**
 * Add to the current stack offset
 */
void x64CodeGenerator::addToCurrentOffset(size_t value)
{
  size_t currentOffset = gStackOffset.top();
  gStackOffset.pop();
  gStackOffset.push(currentOffset + value);
}

/*-------------------------------------------------
 * Register
 *-------------------------------------------------
 */

std::shared_ptr<Register> x64CodeGenerator::getAvailable32BitRegister()
{
  if (gAvailable32BitRegisters.size() == 0) return nullptr;
  std::shared_ptr<Register> reg = gAvailable32BitRegisters.back();
  gAvailable32BitRegisters.pop_back();
  return reg;
}

void x64CodeGenerator::release32BitRegister(std::shared_ptr<Register> reg)
{
  gAvailable32BitRegisters.push_back(reg);
}


/**
 * Loads address object into register
 */
std::shared_ptr<Register> x64CodeGenerator::loadRegister(Address *address, std::shared_ptr<Register> reg)
{
  if (address->IsImmediateOperand()) {
    return loadRegister(static_cast<Int32Address *>(address), reg);
  }
  return loadRegister(static_cast<IdentifierAddress *>(address), reg);
}

/**
 * Loads an integer address into a register
 */
std::shared_ptr<Register> x64CodeGenerator::loadRegister(Int32Address *integerAddress, std::shared_ptr<Register> reg)
{
  std::shared_ptr<ImmediateOperand> immediateOp = std::make_shared<ImmediateOperand>(integerAddress->GetValue(), integerAddress->GetSize());
  if (reg == nullptr) {
    reg = getAvailable32BitRegister();
  }
  gAssembly->AddInstruction(std::make_shared<MoveInstruction>((OpSize)reg->GetSize(), immediateOp, reg));
  return reg;
}

/**
 * Loads a local variable into a register
 */
std::shared_ptr<Register> x64CodeGenerator::loadRegister(IdentifierAddress *identifierAddress, std::shared_ptr<Register> reg)
{
  VariableData *data = gScopes.TopFrame()->GetDataForId(identifierAddress->GetIdentifier());
  if (reg == nullptr) {
    reg = getAvailable32BitRegister()->RegisterForSize((OpSize)data->GetSize());
  }
  
  if (reg != nullptr) {
    auto var = std::make_shared<BaseRelativeOperand>(Register::Rbp.GetShared(), data->GetOffset(), data->GetSize());
    
    // make size the bigger of the register or the id. Sometimes we might want to put a 32 bit into a 64 bit reg.
    size_t size = data->GetSize() >= reg->GetSize() ? data->GetSize() : reg->GetSize();
    
    auto moveInstruction = std::make_shared<MoveInstruction>((OpSize)size, var, reg);
    
    gAssembly->AddInstruction(moveInstruction);
  }
  return reg;
}

std::shared_ptr<Register> x64CodeGenerator::loadEffectiveAddress(IdentifierAddress *identifierAddress, std::shared_ptr<Register> reg)
{
  if (reg == nullptr) {
    reg = getAvailable32BitRegister()->GetQuadWordRegister();
  }
  
  std::shared_ptr<BaseRelativeOperand> idLocation = getBaseRelativeOperand(identifierAddress);
  
  gAssembly->AddInstruction(std::make_shared<LeaInstruction>(OpSizeQuadWord, idLocation, reg));
  return reg;
}

std::shared_ptr<BaseRelativeOperand> x64CodeGenerator::getBaseRelativeOperand(IdentifierAddress *identifier)
{
  VariableData *data = gScopes.TopFrame()->GetDataForId(identifier->GetIdentifier());
  return std::make_shared<BaseRelativeOperand>(Register::Rbp.GetShared(), data->GetOffset(), data->GetSize());
}

std::shared_ptr<ArrayAccessOperand> x64CodeGenerator::getArrayAccessOperand(IdentifierAddress *identifier)
{
  VariableData *vData = gScopes.TopFrame()->GetDataForId(identifier->GetIdentifier());
  const ArrayType *type = static_cast<ArrayType *>((Type *)vData->GetType());
  
  std::shared_ptr<Register> offsetReg = loadRegister((Address *)identifier->GetOffset())->GetQuadWordRegister();
  std::shared_ptr<Register> arrayReg = loadEffectiveAddress(identifier);
  return std::make_shared<ArrayAccessOperand>(arrayReg, offsetReg, type->GetBaseType()->GetTypeSize());
}

void x64CodeGenerator::push(Address *address)
{
  if (address->IsImmediateOperand()) {
    push(static_cast<Int32Address *>(address));
  } else {
    push(static_cast<IdentifierAddress *>(address));
  }
}

void x64CodeGenerator::push(IdentifierAddress *identifierAddress)
{
  auto baseRelativeOp = getBaseRelativeOperand(identifierAddress);
  std::shared_ptr<PushInstruction> pushInstruction = std::make_shared<PushInstruction>((OpSize)baseRelativeOp->GetSize(), baseRelativeOp);
  gAssembly->AddInstruction(pushInstruction);
  
  size_t offset = gStackOffset.top();
  gStackOffset.pop();
  offset += baseRelativeOp->GetSize();
  gStackOffset.push(offset);
}

void x64CodeGenerator::push(Int32Address *integerAddress)
{
  std::shared_ptr<ImmediateOperand> imm = std::make_shared<ImmediateOperand>(integerAddress->GetValue(), integerAddress->GetSize());
  std::shared_ptr<PushInstruction> pushInstruction = std::make_shared<PushInstruction>((OpSize)integerAddress->GetSize(), imm);
  gAssembly->AddInstruction(pushInstruction);
  
  size_t offset = gStackOffset.top();
  gStackOffset.pop();
  offset += integerAddress->GetSize();
  gStackOffset.push(offset);
}

void x64CodeGenerator::push(std::shared_ptr<Register> reg)
{
  std::shared_ptr<PushInstruction> pushInstruction = std::make_shared<PushInstruction>((OpSize)reg->GetSize(), reg);
  gAssembly->AddInstruction(pushInstruction);
  
  size_t offset = gStackOffset.top();
  gStackOffset.pop();
  offset += reg->GetSize();
  gStackOffset.push(offset);
}

void x64CodeGenerator::pop(std::shared_ptr<Register> reg)
{
  std::shared_ptr<PopInstruction> popInstruction = std::make_shared<PopInstruction>((OpSize)reg->GetSize(), reg);
  gAssembly->AddInstruction(popInstruction);
  
  size_t offset = gStackOffset.top();
  gStackOffset.pop();
  offset -= reg->GetSize();
  gStackOffset.push(offset);
}

std::shared_ptr<Label> x64CodeGenerator::createLabelFromLucyLabel(LucyLabel *labelIn) const
{
  std::string labelName = labelIn->GetIdentifer()->GetIdentifier();
  std::string labelOutName = "_" + gCurrentFunctionId + "_" + labelName;
  return std::make_shared<Label>(labelOutName);
}

bool x64CodeGenerator::isRegisterInUse(std::shared_ptr<Register> reg) const
{
  for (auto availableReg : gAvailable32BitRegisters) {
    if (reg->IsEqual(availableReg)) return false;
  }
  return true;
}

#endif
