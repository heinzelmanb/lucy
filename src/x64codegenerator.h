#pragma once

#ifdef __x86_64

#include <string>
#include <memory>
#include <vector>
#include <stack>
#include <map>

#include "visitor.h"
#include "x64instructions.h"
#include "x64assembly.h"
#include "ast.h"
#include "symboltable.h"

namespace lcy
{
  namespace x64
  {
    class x64CodeGenerator : public Visitor
    {
    public:
      x64CodeGenerator();
      ~x64CodeGenerator();
      std::shared_ptr<lcy::Assembly> GenerateCode(Program *program);
      
      // visitor methods
      virtual void Visit(Program                          *program);
      virtual void Visit(FunctionDeclaration              *functionDecl);
      virtual void Visit(StatementList                    *statementList);
      virtual void Visit(IdentifierAddress                *address);
      virtual void Visit(Int32Address                     *address);
      virtual void Visit(AssignmentStatement              *statement);
      virtual void Visit(VariableInitializationStatement  *statement);
      virtual void Visit(ParameterList                    *parameterList);
      virtual void Visit(ReturnStatement                  *statement);
      virtual void Visit(CallStatement                    *callStatement);
      virtual void Visit(SimpleExpression                 *simpleExpression);
      virtual void Visit(ComplexExpression                *complexExpression);
      virtual void Visit(LucyLabel                        *label);
      virtual void Visit(IfStatement                      *statement);
      virtual void Visit(GotoStatement                    *statement);
      
    private:
      std::shared_ptr<X64Assembly> gAssembly;
      
      std::string gCurrentFunctionId;

      std::stack<size_t> gStackOffset;
      // key is the variable name, value is a Variable Data Object
      SymbolTable gScopes;
      
      std::vector<std::shared_ptr<Register>> gArgument32BitRegisters;
      
      std::vector<std::shared_ptr<Register>> gAvailable32BitRegisters;
    private:
      void assign(IdentifierAddress *destination, ComplexExpression *expression);
      void assign(IdentifierAddress *destination, SimpleExpression *expression, TokenType prefixOperator);
      
      // will perform prefix operation on operand
      std::shared_ptr<Operand> evalPrefixOperator(IdentifierAddress *operand, TokenType prefixOperator, std::vector<std::shared_ptr<Register>> &registersToFree);
      
      std::shared_ptr<ImmediateOperand> evalPrefixOperator(Int32Address *address, TokenType prefixOperator);
      
      void evaluateExpression(std::shared_ptr<Operand> source,
                              std::shared_ptr<Operand> destination,
                              TokenType op,
                              std::shared_ptr<Label> jmpLabel=nullptr);
      
      void evaluateRelationalExpression(std::shared_ptr<Register> left,
                                        std::shared_ptr<Register> right,
                                        TokenType op,
                                        std::shared_ptr<Label> jmpLabel=nullptr);
      
      std::shared_ptr<Label> generateFunctionExitLabel();
      void generateFunctionDirectives(FunctionDeclaration *functionDecl);
      void generateReturnInstruction();
      void addToCurrentOffset(size_t value);
      
      std::shared_ptr<Register> getAvailable32BitRegister();
      void release32BitRegister(std::shared_ptr<Register> reg);
      bool isRegisterInUse(std::shared_ptr<Register> reg) const;
      
      std::shared_ptr<Register> loadRegister(Address *address, std::shared_ptr<Register> reg=nullptr);
      std::shared_ptr<Register> loadRegister(IdentifierAddress *identifierAddress, std::shared_ptr<Register> reg=nullptr);
      std::shared_ptr<Register> loadRegister(Int32Address *integerAddress, std::shared_ptr<Register> reg=nullptr);
      
      /**
       * Will load address of id into register. NOTE: will return 64bit reg.
       */
      std::shared_ptr<Register> loadEffectiveAddress(IdentifierAddress *identifierAddress, std::shared_ptr<Register> reg=nullptr);
      
      void push(Address *address);
      void push(IdentifierAddress *identifierAddress);
      void push(Int32Address *integerAddress);
      void push(std::shared_ptr<Register> reg);
      
      void pop(std::shared_ptr<Register> reg);
      
      std::shared_ptr<Label> createLabelFromLucyLabel(LucyLabel *labelIn) const;
      
      std::shared_ptr<BaseRelativeOperand> getBaseRelativeOperand(IdentifierAddress *identifier);
      std::shared_ptr<ArrayAccessOperand> getArrayAccessOperand(IdentifierAddress *identifier);
    };
  }
}

#endif
