#include "parser.h"
#include "exceptions.h"
#include "object.h"

using namespace lcy;

Parser::Parser(std::shared_ptr<Lexer> lexer) : gLexer(lexer)
{
}

Parser::~Parser()
{
}

std::unique_ptr<Program> Parser::Parse()
{
  return declList();
}

std::unique_ptr<Program> Parser::declList()
{
  std::unique_ptr<Program> programPtr = nullptr;
  if (gLexer->PeekNextToken().GetType() != TokenType::Eof) {
    std::unique_ptr<Program> program = MakeUnique<Program>();
    std::unique_ptr<Declaration> declaration = decl();
    std::unique_ptr<Program> otherProgram = declList();
    
    if (declaration != nullptr) {
      program->AppendDeclaration(std::move(declaration));
    }
    if (otherProgram != nullptr) {
      program->AppendDeclarationList(std::move(otherProgram));
    }
    programPtr = std::move(program);
  }
  return programPtr;
}

std::unique_ptr<Declaration> Parser::decl()
{
  if (gLexer->PeekNextToken().GetType() == TokenType::FuncKeyword) {
    return functionDecl();
  } else {
    assertFail("Expected a declaration");
  }
  return nullptr;
}

std::unique_ptr<FunctionDeclaration> Parser::functionDecl()
{
  assertTrue(gLexer->GetNextToken().GetType() == TokenType::FuncKeyword, "Expected `func` keyword");
  
  auto token = gLexer->GetNextToken();
  assertTrue(token.GetType() == TokenType::Id, "Expected an id");
  std::unique_ptr<IdentifierAddress> id = MakeUnique<IdentifierAddress>(token.GetValue());
  std::unique_ptr<ParameterList> plist = parameterList();
  assertTrue(gLexer->GetNextToken().GetType() == TokenType::Tilde, "Expected return type");
  std::unique_ptr<Type> functionType = type();
  std::unique_ptr<StatementList> stmtList = statementList();
  
  return MakeUnique<FunctionDeclaration>(std::move(id),
                                         std::move(plist),
                                         std::move(functionType),
                                         std::move(stmtList));
}

std::unique_ptr<lcy::ParameterList> Parser::parameterList()
{
  std::unique_ptr<ParameterList> plist = nullptr;
  assertTrue(gLexer->GetNextToken().GetType() == TokenType::LeftParen, "Expected `(`");
  if (gLexer->PeekNextToken().GetType() == TokenType::DeclKeyword) {
    plist = MakeUnique<ParameterList>(ParameterList());
    plist->AppendParameter(variableDeclStatement());
    plist->AppendParameters(parameterListTail());
  }
  assertTrue(gLexer->GetNextToken().GetType() == TokenType::RightParen, "Expected `)`");
  return plist;
}

std::unique_ptr<lcy::ParameterList> Parser::parameterListTail()
{
  std::unique_ptr<ParameterList> plist = nullptr;
  if (gLexer->PeekNextToken().GetType() != TokenType::RightParen) {
    assertTrue(gLexer->GetNextToken().GetType() == TokenType::Comma, "Expected a `,`");
    plist = MakeUnique<ParameterList>(ParameterList());
    plist->AppendParameter(variableDeclStatement());
    plist->AppendParameters(parameterListTail());
  }
  return plist;
}

std::unique_ptr<StatementList> Parser::statementList()
{
  assertTrue(gLexer->GetNextToken().GetType() == TokenType::BeginKeyword, "Expected `begin`");
  auto statementList = statementListTail();
  assertTrue(gLexer->GetNextToken().GetType() == TokenType::EndKeyword, "Expected `end`");
  return statementList;
}

std::unique_ptr<StatementList> Parser::statementListTail()
{
  std::unique_ptr<StatementList> statementList = nullptr;
  if (gLexer->PeekNextToken().GetType() != TokenType::EndKeyword) {
    statementList = MakeUnique<StatementList>(StatementList());
    
    auto stmt = statement();
    auto moreStmts = statementListTail();
    
    if (stmt != nullptr) {
      statementList->AppendStatement(std::move(stmt));
    }
    
    if (moreStmts != nullptr) {
      statementList->AppendStatements(std::move(moreStmts));
    }
  }
  return statementList;
}

std::unique_ptr<Statement> Parser::statement()
{
  std::unique_ptr<Statement> statement = nullptr;
  if (gLexer->PeekNextToken().GetType() == TokenType::DeclKeyword) {
    statement = variableDeclStatement();
  } else if (gLexer->PeekNextToken().GetType() == TokenType::ReturnKeyword) {
    statement = returnStatement();
  } else if (gLexer->PeekNextToken().GetType() == TokenType::CallKeyword) {
    statement = callStatement();
  } else if (gLexer->PeekNextToken().GetType() == TokenType::At) {
    statement = labelStatement();
  } else if (gLexer->PeekNextToken().GetType() == TokenType::IfKeyword) {
    statement = ifStatement();
  } else if (gLexer->PeekNextToken().GetType() == TokenType::GotoKeyword) {
    statement = gotoStatement();
  } else {
    statement = assignmentStatement();
  }
  
  assertTrue(gLexer->GetNextToken().GetType() == TokenType::Semicolon);
  
  return statement;
}

std::unique_ptr<Statement> Parser::assignmentStatement()
{
  TokenType prefixOp = TokenType::None;
  
  if (gLexer->PeekNextToken().IsPrefixOperator()) {
    prefixOp = gLexer->GetNextToken().GetType();
  }
  
  std::unique_ptr<IdentifierAddress> idAddr = identifierAddress(true);
  assertTrue(gLexer->GetNextToken().GetType() == TokenType::Assign, "Expected an assignment");
  return MakeUnique<AssignmentStatement>(std::move(idAddr), expression(), prefixOp);
}

std::unique_ptr<lcy::Expression> Parser::expression()
{
  if (gLexer->PeekNextToken().GetType() == TokenType::CallKeyword) {
    return callStatement();
  } else {
    
    TokenType prefixOperator = TokenType::None;
    
    if (gLexer->PeekNextToken().IsPrefixOperator()) {
      prefixOperator = gLexer->GetNextToken().GetType();
    }
    
    std::unique_ptr<Address> lhs = address();
    
    if (gLexer->PeekNextToken().IsOperator() && prefixOperator == TokenType::None) {
      // is the form x = 1 + 1
      const Token op = gLexer->GetNextToken();
      std::unique_ptr<Address> rhs = address();
      
      return MakeUnique<ComplexExpression>(std::move(lhs), std::move(rhs), op.GetType());
      
    } else {
      // is the form x = 1
      return MakeUnique<SimpleExpression>(std::move(lhs), prefixOperator);
    }
  }
}

std::unique_ptr<VariableInitializationStatement> Parser::variableDeclStatement()
{
  assertTrue(gLexer->GetNextToken().GetType() == TokenType::DeclKeyword, "Expected Decl");
  std::unique_ptr<Type> variableType = type();
  auto token = gLexer->GetNextToken();
  assertTrue(token.GetType() == TokenType::Id, "Expected an Id");
  
  std::unique_ptr<IdentifierAddress> id = MakeUnique<IdentifierAddress>(IdentifierAddress(token.GetValue()));
  
  return MakeUnique<VariableInitializationStatement>(std::move(variableType), std::move(id));
}

std::unique_ptr<ReturnStatement> Parser::returnStatement()
{
  std::unique_ptr<ReturnStatement> returnStmt = MakeUnique<ReturnStatement>();
  assertTrue(gLexer->GetNextToken().GetType() == TokenType::ReturnKeyword);
  if (gLexer->PeekNextToken().GetType() != TokenType::Semicolon) {
    std::unique_ptr<Address> returnValue = address();
    returnStmt->SetReturnAddressValue(std::move(returnValue));
  }
  return returnStmt;
}

std::unique_ptr<lcy::LucyLabel> Parser::labelStatement()
{
  assertTrue(gLexer->GetNextToken().GetType() == TokenType::At);
  auto idToken = gLexer->GetNextToken();
  assertTrue(idToken.GetType() == TokenType::Id);
  auto idAddress = MakeUnique<IdentifierAddress>(idToken.GetValue());
  return MakeUnique<LucyLabel>(std::move(idAddress));
}

std::unique_ptr<lcy::IfStatement> Parser::ifStatement()
{
  assertTrue(gLexer->GetNextToken().GetType() == TokenType::IfKeyword);
  std::unique_ptr<Expression> exp = expression();
  assertTrue(gLexer->GetNextToken().GetType() == TokenType::GotoKeyword);
  std::unique_ptr<LucyLabel> label = labelStatement();
  return MakeUnique<IfStatement>(std::move(exp), std::move(label));
}

std::unique_ptr<lcy::GotoStatement> Parser::gotoStatement()
{
  assertTrue(gLexer->GetNextToken().GetType() == TokenType::GotoKeyword);
  std::unique_ptr<LucyLabel> label = labelStatement();
  return MakeUnique<GotoStatement>(std::move(label));
}

std::unique_ptr<CallStatement> Parser::callStatement()
{
  assertTrue(gLexer->GetNextToken().GetType() == TokenType::CallKeyword);
  
  auto idToken = gLexer->GetNextToken();
  assertTrue(idToken.GetType() == TokenType::Id);

  std::unique_ptr<CallStatement> callStmt = MakeUnique<CallStatement>(MakeUnique<IdentifierAddress>(idToken.GetValue()));
  
  assertTrue(gLexer->GetNextToken().GetType() == TokenType::LeftParen);
  
  bool checkForComma = false;
  
  while (gLexer->PeekNextToken().GetType() != TokenType::RightParen) {
    if (checkForComma) {
      assertTrue(gLexer->GetNextToken().GetType() == TokenType::Comma);
    }
  
    callStmt->AppendArguments(address());
    checkForComma = true;
    
    if (gLexer->PeekNextToken().GetType() == TokenType::Eof || gLexer->PeekNextToken().GetType() == TokenType::Error) {
      assertFail();
    }
  }
  
  assertTrue(gLexer->GetNextToken().GetType() == TokenType::RightParen);
  
  return callStmt;
}

std::unique_ptr<Address> Parser::address(bool allowOffset)
{
  if (gLexer->PeekNextToken().GetType() == TokenType::Id) {
    return identifierAddress(allowOffset);
  } else if (gLexer->PeekNextToken().GetType() == TokenType::Integer32) {
    Int32 value = atoi(gLexer->GetNextToken().GetValue().c_str());
    return MakeUnique<Int32Address>(value);
  } else {
    assertFail("Expected an address");
  }
  return nullptr;
}

std::unique_ptr<lcy::IdentifierAddress> Parser::identifierAddress(bool allowOffset)
{
  Token token = gLexer->GetNextToken();
  assertTrue(token.GetType() == TokenType::Id);
  std::unique_ptr<Address> offset = nullptr;
  if (gLexer->PeekNextToken().GetType() == TokenType::LeftBracket && allowOffset) {
    gLexer->GetNextToken();
    offset = address(false);
    assertTrue(gLexer->GetNextToken().GetType() == TokenType::RightBracket);
  }
  return MakeUnique<IdentifierAddress>(token.GetValue(), std::move(offset));
}

std::unique_ptr<Type> Parser::type()
{
  assertTrue(gLexer->GetNextToken().GetType() == TokenType::Int32Keyword, "Expected a Type");
 
  std::unique_ptr<Type> baseType = MakeUnique<PrimitiveType>(PrimitiveType::IntType);
  if (gLexer->PeekNextToken().GetType() == TokenType::LeftBracket) {
    return arrayType(std::move(baseType));
  } else if (gLexer->PeekNextToken().GetType() == TokenType::Multiply) {
    return pointerType(std::move(baseType));
  }
  return baseType;
}

std::unique_ptr<PointerType> Parser::pointerType(std::unique_ptr<Type> baseType)
{
  assertTrue(gLexer->GetNextToken().GetType() == TokenType::Multiply);
  std::unique_ptr<PointerType> ptrType = MakeUnique<PointerType>(std::move(baseType));
  
  if (gLexer->PeekNextToken().GetType() == TokenType::Multiply) {
    return pointerType(std::move(ptrType));
  }
  
  return ptrType;
}

std::unique_ptr<ArrayType> Parser::arrayType(std::unique_ptr<Type> baseType)
{
  assertTrue(gLexer->GetNextToken().GetType() == TokenType::LeftBracket);
  
  Token countToken = gLexer->GetNextToken();
  Token nextToken(TokenType::None);
  
  size_t count;
  if (countToken.GetType() == TokenType::Integer32) {
    count = atoi(countToken.GetValue().c_str());
    nextToken = gLexer->GetNextToken();
  } else {
    count = 0;
    nextToken = countToken;
  }
  
  assertTrue(nextToken.GetType() == TokenType::RightBracket);
  
  return MakeUnique<ArrayType>(std::move(baseType), count);
}

void Parser::assertTrue(bool expression, std::string errorMessage)
{
  if (expression == false) {
    throw ParseError(errorMessage, gLexer->GetCurrentToken().GetLineNumber());
  }
}

void Parser::assertFail(std::string errorMessage)
{
  throw ParseError(errorMessage, gLexer->GetCurrentToken().GetLineNumber());
}
