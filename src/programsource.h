#pragma once

namespace lcy
{

class ProgramSource
{
public:
  virtual ~ProgramSource()
  {
  }
  
  virtual char NextChar() = 0;
  virtual char PeekChar() = 0;
  virtual bool Eof() = 0;
  virtual void SkipWhitespace() = 0;
  virtual int GetLineNumber() const = 0;
};
  
  
}
