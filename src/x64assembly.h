#pragma once

#ifdef __x86_64

#include "asm.h"
#include "x64instructions.h"

namespace lcy
{
  namespace x64
  {
    class X64Assembly : public Assembly {
    public:
      X64Assembly();
      ~X64Assembly();
      
      void AddInstruction(std::shared_ptr<Instruction> instruction);
      void AddLabel(std::shared_ptr<Label> label);
      void AddLabel(std::string labelName);
      void AddDirective(std::shared_ptr<Directive> directive);
      void AddJmp(std::shared_ptr<JmpInstruction> jmpInstruction);
    };
  }
}

#endif
