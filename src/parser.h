#pragma once

#include "lexer.h"
#include "token.h"
#include "ast.h"

#include <memory>

namespace lcy
{

class Parser
{
public:
  Parser(std::shared_ptr<lcy::Lexer> lexer);
  ~Parser();
  
  std::unique_ptr<lcy::Program> Parse();
  
private:
  std::shared_ptr<lcy::Lexer> gLexer;
  
private:
  std::unique_ptr<lcy::Program> declList();
  std::unique_ptr<lcy::Declaration> decl();
  std::unique_ptr<lcy::FunctionDeclaration> functionDecl();
  
  std::unique_ptr<lcy::ParameterList> parameterList();
  std::unique_ptr<lcy::ParameterList> parameterListTail();
  
  std::unique_ptr<lcy::StatementList> statementList();
  std::unique_ptr<lcy::StatementList> statementListTail();
  std::unique_ptr<lcy::Statement> statement();
  
  // address can either be an Id, or an Integer, for now.
  std::unique_ptr<lcy::Address> address(bool allowOffset=true);
  std::unique_ptr<lcy::IdentifierAddress> identifierAddress(bool allowOffset);
  std::unique_ptr<lcy::VariableInitializationStatement> variableDeclStatement();
  std::unique_ptr<lcy::Statement> assignmentStatement();
  std::unique_ptr<lcy::ReturnStatement> returnStatement();
  std::unique_ptr<lcy::CallStatement> callStatement();
  std::unique_ptr<lcy::LucyLabel> labelStatement();
  std::unique_ptr<lcy::IfStatement> ifStatement();
  std::unique_ptr<lcy::GotoStatement> gotoStatement();
  std::unique_ptr<lcy::Expression> expression();
  
  std::unique_ptr<Type> type();
  std::unique_ptr<PointerType> pointerType(std::unique_ptr<Type> baseType);
  std::unique_ptr<ArrayType> arrayType(std::unique_ptr<Type> baseType);
  
  void assertTrue(bool expression, std::string message="");
  void assertFail(std::string message="");
};

}
