#pragma once

#include <string>
#include <memory>

namespace lcy
{
  
  class CompilerOptions
  {
  public:
    CompilerOptions()
    {
      outputToStdOut = false;
      writeToFile = true;
      outputFile = "";
      run = false;
    }
    
    bool outputToStdOut;
    bool writeToFile;
    bool run;
    std::string outputFile;
  };

  class Compiler {
  public:
    Compiler(std::shared_ptr<CompilerOptions> options);
    bool Compile(std::string &filename);
  private:
    std::shared_ptr<CompilerOptions> gOptions;
  };
  
}
