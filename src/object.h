#pragma once

#include <memory>
#include <vector>

namespace lcy
{
  
  template<typename T, typename... Args>
  static std::unique_ptr<T> MakeUnique(Args&&... args) {
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
  }
  
  template<typename T>
  static inline std::vector<T*> GetPointerVector(const std::vector<std::unique_ptr<T>> &vector)
  {
    std::vector<T*> ptrs;
    for (auto const &ptr : vector) {
      ptrs.push_back(ptr.get());
    }
    return ptrs;
  }
}
  
