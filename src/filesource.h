#pragma once

#include "programsource.h"

#include <fstream>

namespace lcy
{

class FileSource : public ProgramSource
{
public:
  FileSource(std::ifstream *inputFile);
  FileSource(const FileSource &source);
  virtual ~FileSource() {}
  char NextChar();
  char PeekChar();
  bool Eof();
  void SkipWhitespace();
  
  int GetLineNumber() const;
  
  std::ifstream *GetStream() const;

private:
  std::ifstream *gInputFile;
  int gLineNumber;
};
  
}
