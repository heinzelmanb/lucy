#include "filesource.h"

#include <locale>

using namespace lcy;

FileSource::FileSource(std::ifstream *input) : gInputFile(input)
{
  gLineNumber = 0;
}

FileSource::FileSource(const FileSource &source)
{
  gLineNumber = 0;
  this->gInputFile = source.GetStream();
}

bool FileSource::Eof()
{
  return gInputFile->eof();
}

char FileSource::NextChar()
{
  char c = gInputFile->get();
  if (c == '\n') gLineNumber++;
  return c;
}

char FileSource::PeekChar()
{
  return gInputFile->peek();
}

void FileSource::SkipWhitespace()
{
  while (isspace(PeekChar())) {
    NextChar();
  }
}

std::ifstream *FileSource::GetStream() const
{
  return gInputFile;
}

int FileSource::GetLineNumber() const
{
  return gLineNumber;
}
