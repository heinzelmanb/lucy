#pragma once

/**
 * File for exceptions to live.
 */

namespace lcy
{
class Exception : public std::exception {
public:
  Exception() {};
  
  Exception(const char* msg) : err_msg(msg) {};
  
  ~Exception() throw() {};
  
  virtual const char *what() const throw() { return this->err_msg.c_str(); };
protected:
  std::string err_msg;
};

/**
 * Should be thrown when a parse error occurs
 */
class ParseError : public Exception {
public:
  ParseError(std::string msg, unsigned int line) {
    err_msg = "Parse Error: " + msg + "; on line " + std::to_string(line);
  }
  
  ~ParseError() throw() {};
};
  
/**
 * Should be thrown when a parse error occurs
 */
class TypeError : public Exception {
public:
  TypeError(std::string msg, unsigned int line) {
    err_msg = "Type Error: " + msg + "; on line " + std::to_string(line);
  }
  
  ~TypeError() throw() {};
};


}


