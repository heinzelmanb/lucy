#pragma once

#ifdef __x86_64

#include <string>
#include <cassert>

#include "object.h"
#include "register.h"
#include "asm.h"

namespace lcy
{
 
  namespace x64
  {
    typedef const std::string InstructionName;

    extern InstructionName kInstructionPush;
    extern InstructionName kInstructionPop;
    extern InstructionName kInstructionMove;
    extern InstructionName kInstructionLea;
    extern InstructionName kInstructionReturn;
    extern InstructionName kInstructionAdd;
    extern InstructionName kInstructionSubtract;
    extern InstructionName kInstructionJmp;
    extern InstructionName kInstructionCall;
    extern InstructionName kInstructiomCmp;
    extern InstructionName kInstructionSetE;
    extern InstructionName kInstructionSetG;
    extern InstructionName kInstructionSetGE;
    extern InstructionName kInstructionSetLE;
    extern InstructionName kInstructionSetL;
    extern InstructionName kInstructionSetNE;
    extern InstructionName kInstructinJE;
    extern InstructionName kInstructionJNE;
    extern InstructionName kInstructinJG;
    extern InstructionName kInstructinJGE;
    extern InstructionName kInstructinJLE;
    extern InstructionName kInstructinJL;
    extern InstructionName kInstructionIMul;
    extern InstructionName kInstructionNeg;

    // Forward Declarations
    class Operand;
    class Label;
    
    
    /*=================================
     * Instructions
     *=================================
     */
    class Instruction : public AssemblyElement
    {
    public:
      Instruction(OpSize size);
      virtual std::string ToString() const = 0;
      virtual InstructionName GetInstructionName() const = 0;
    protected:
      OpSize gSize;
      
    protected:
      std::string getSizeSuffix() const;
    };
    
    class NoOpInstruction : public Instruction
    {
    public:
      NoOpInstruction() : Instruction(OpSizeNone) {}
      std::string ToString() const;
      virtual InstructionName GetInstructionName() const = 0;
    };
    
    class ReturnInstruction : public NoOpInstruction
    {
    public:
      ReturnInstruction()
      {
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructionReturn;
      }
    };
    
    class OneOpInstruction : public Instruction
    {
    public:
      OneOpInstruction(OpSize size, std::shared_ptr<Operand> op);
      std::string ToString() const;
      virtual InstructionName GetInstructionName() const = 0;
    protected:
      std::shared_ptr<Operand> gOperand;
    };
    
    class TwoOpInstruction : public Instruction
    {
    public:
      TwoOpInstruction(OpSize size,
                       std::shared_ptr<Operand> opOne,
                       std::shared_ptr<Operand> opTwo);
      
      virtual InstructionName GetInstructionName() const = 0;
      
      std::string ToString() const;
    protected:
      std::shared_ptr<Operand> gOperandOne;
      std::shared_ptr<Operand> gOperandTwo;
      OpSize gSize;
    };
    
    class PushInstruction : public OneOpInstruction
    {
    public:
      PushInstruction(OpSize size, std::shared_ptr<Operand> op) : OneOpInstruction(size, op)
      {
        assert(size == OpSizeQuadWord);
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructionPush;
      }
    };
    
    class PopInstruction : public OneOpInstruction
    {
    public:
      PopInstruction(OpSize size, std::shared_ptr<Operand> op) : OneOpInstruction(size, op)
      {
        assert(size == OpSizeQuadWord);
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructionPop;
      }
    };
    
    class MoveInstruction : public TwoOpInstruction
    {
    public:
      MoveInstruction(OpSize size,
                       std::shared_ptr<Operand> opOne,
                       std::shared_ptr<Operand> opTwo) : TwoOpInstruction(size, opOne, opTwo)
      {
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructionMove;
      }
    };
    
    class LeaInstruction : public TwoOpInstruction
    {
    public:
      LeaInstruction(OpSize size,
                      std::shared_ptr<Operand> opOne,
                      std::shared_ptr<Operand> opTwo) : TwoOpInstruction(size, opOne, opTwo)
      {
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructionLea;
      }
    };
    
    class SetInstruction : public OneOpInstruction
    {
    public:
      SetInstruction(std::shared_ptr<Register> reg) : OneOpInstruction(OpSizeNone, reg)
      {
        assert(reg->GetSize() == OpSizeByte);
      }
    };
    
    class SetEInstruction : public SetInstruction
    {
    public:
      SetEInstruction(std::shared_ptr<Register> op) : SetInstruction(op)
      {
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructionSetE;
      }
    };
    
    class SetNEInstruction : public SetInstruction
    {
    public:
      SetNEInstruction(std::shared_ptr<Register> op) : SetInstruction(op)
      {
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructionSetNE;
      }
    };
    
    class SetGInstruction : public SetInstruction
    {
    public:
      SetGInstruction(std::shared_ptr<Register> op) : SetInstruction(op)
      {
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructionSetG;
      }
    };
    
    class SetGEInstruction : public SetInstruction
    {
    public:
      SetGEInstruction(std::shared_ptr<Register> op) : SetInstruction(op)
      {
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructionSetGE;
      }
    };
    
    class SetLInstruction : public SetInstruction
    {
    public:
      SetLInstruction(std::shared_ptr<Register> op) : SetInstruction(op)
      {
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructionSetL;
      }
    };
    
    class SetLEInstruction : public SetInstruction
    {
    public:
      SetLEInstruction(std::shared_ptr<Register> op) : SetInstruction(op)
      {
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructionSetLE;
      }
    };
    
    class AddInstruction : public TwoOpInstruction
    {
    public:
      AddInstruction(OpSize size,
                      std::shared_ptr<Operand> opOne,
                      std::shared_ptr<Operand> opTwo) : TwoOpInstruction(size, opOne, opTwo)
      {
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructionAdd;
      }
    };
    
    class IMulInstruction : public TwoOpInstruction
    {
    public:
      IMulInstruction(OpSize size,
                      std::shared_ptr<Operand> opOne,
                      std::shared_ptr<Operand> opTwo) : TwoOpInstruction(size, opOne, opTwo)
      {
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructionIMul;
      }
      
    };
    
    class SubtractInstruction : public TwoOpInstruction
    {
    public:
      SubtractInstruction(OpSize size,
                          std::shared_ptr<Operand> opOne,
                          std::shared_ptr<Operand> opTwo) : TwoOpInstruction(size, opOne, opTwo)
      {
      }
      
      InstructionName GetInstructionName() const {
        return kInstructionSubtract;
      }
    };
    
    class CmpInstruction : public TwoOpInstruction
    {
    public:
      CmpInstruction(OpSize size,
                     std::shared_ptr<Register> opOne,
                     std::shared_ptr<Register> opTwo) : TwoOpInstruction(size, opOne, opTwo)
      {
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructiomCmp;
      }
    };
    
    /*=================================
     * Other program elements
     *=================================
     */
    class Label : public AssemblyElement
    {
    public:
      Label(std::string labelName);
      
      std::string ToString() const;
      
      std::string GetLabelName() const;
    private:
      std::string gLabelName;
    };
    
    class Directive : public AssemblyElement
    {
    public:
      Directive(std::string directive);
      std::string ToString() const;
    
    private:
      std::string gDirective;
    };
    
    class NegInstruction : public OneOpInstruction
    {
    public:
      NegInstruction(OpSize size, std::shared_ptr<Operand> op) : OneOpInstruction(size, op)
      {
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructionNeg;
      }
    };
    
    /*=================================
     * Jump Instructions
     *=================================
     */
    class JmpInstruction : public Instruction
    {
    public:
      JmpInstruction(std::shared_ptr<Label> label) : Instruction(OpSizeNone)
      {
        gJmpLabel = label;
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructionJmp;;
      }
      
      std::string ToString() const
      {
        return GetInstructionName() + " " + gJmpLabel->GetLabelName();
      }
      
    private:
      std::shared_ptr<Label> gJmpLabel;
    };
    
    class JmpEInstruction : public JmpInstruction
    {
    public:
      JmpEInstruction(std::shared_ptr<Label> label) : JmpInstruction(label)
      {
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructinJE;
      }
    };
    
    class JmpNEInstruction : public JmpInstruction
    {
    public:
      JmpNEInstruction(std::shared_ptr<Label> label) : JmpInstruction(label)
      {
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructionJNE;
      }
    };
    
    class JmpGInstruction : public JmpInstruction
    {
    public:
      JmpGInstruction(std::shared_ptr<Label> label) : JmpInstruction(label)
      {
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructinJG;
      }
    };
    
    class JmpGEInstruction : public JmpInstruction
    {
    public:
      JmpGEInstruction(std::shared_ptr<Label> label) : JmpInstruction(label)
      {
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructinJGE;
      }
    };
    
    class JmpLInstruction : public JmpInstruction
    {
    public:
      JmpLInstruction(std::shared_ptr<Label> label) : JmpInstruction(label)
      {
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructinJL;
      }
    };
    
    class JmpLEInstruction : public JmpInstruction
    {
    public:
      JmpLEInstruction(std::shared_ptr<Label> label) : JmpInstruction(label)
      {
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructinJLE;
      }
    };

    class CallInstruction : public Instruction
    {
    public:
      CallInstruction(std::shared_ptr<Label> label) : Instruction(OpSizeQuadWord)
      {
        gLabel = label;
      }
      
      InstructionName GetInstructionName() const
      {
        return kInstructionCall;
      }
      
      std::string ToString() const
      {
        return GetInstructionName() + getSizeSuffix() + " " + gLabel->GetLabelName();
      }
      
    private:
      std::shared_ptr<Label> gLabel;
    };
  }
}

#endif
