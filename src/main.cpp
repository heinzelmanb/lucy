#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <string.h>

#include "compiler.h"
#include "util.h"

std::shared_ptr<lcy::CompilerOptions> parse_cmdline(int argc, char **argv, std::string &file);
void print_cmdline_options();

int main(int argc, char **argv) 
{
  if (argc < 2) {
	  std::cerr << "Error: specify a file to compile" << std::endl;
	  return -1;
  }
  
  std::string filename;
  std::shared_ptr<lcy::CompilerOptions> options = parse_cmdline(argc, argv, filename);
  
  lcy::Compiler compiler(options);
  
  compiler.Compile(filename);
  
  if (options->run) {
    int exitStatus;
    bool success = ExecuteAssembly(options->outputFile, exitStatus);
    if (success) {
      std::cout << "Exit Code: " << exitStatus << std::endl;
    }
  }
}

std::shared_ptr<lcy::CompilerOptions> parse_cmdline(int argc, char **argv, std::string &file)
{
  if (argc == 1) {
    print_cmdline_options();
    return nullptr;
  }
  
  std::shared_ptr<lcy::CompilerOptions> options = std::make_shared<lcy::CompilerOptions>();
  
  for (int i = 1; i < argc; i++) {
    char *arg = argv[i];
    
    if (strcmp(arg, "--stdout") == 0) {
      options->outputToStdOut = true;
      continue;
    }
    
    if (strcmp(arg, "-o") == 0) {
      options->outputFile = std::string(argv[++i]);
      
      if (options->outputFile == "\"\"") {
        options->writeToFile = false;
      }
      continue;
    }
    
    if (strcmp(arg, "-r") == 0) {
      options->run = true;
    }
    
    std::ifstream stream(argv[i]);
    if (stream.good()) {
      file = std::string(argv[i]);
    }
  }
  
  if (options->writeToFile == true && options->outputFile.size() == 0) {
    options->outputFile = file + ".s";
  }
  
  return options;
}

void print_cmdline_options()
{
  std::cout << "OVERVIEW: Lucy Compiler" << std::endl;
  std::cout << "USAGE: lucy [options] input" << std::endl;
  
  std::cout << "OPTIONS:" << std::endl;
  std::cout << "--stdout Writes output to console" << std::endl;
  std::cout << "-o <file> Specify output file, \"\" will not output file" << std::endl;
  std::cout << "-r Run lucy program" << std::endl;
}
