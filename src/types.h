#pragma once

#include <stdint.h>

namespace lcy
{
  typedef int32_t Int32;
  typedef int64_t Int64;
  typedef int16_t Int16;
  typedef unsigned char Byte;
}
