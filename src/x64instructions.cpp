#ifdef __x86_64
#include "x64instructions.h"

using namespace lcy;
using namespace x64;

InstructionName lcy::x64::kInstructionPush           =   "push";
InstructionName lcy::x64::kInstructionMove           =   "mov";
InstructionName lcy::x64::kInstructionLea            =   "lea";
InstructionName lcy::x64::kInstructionPop            =   "pop";
InstructionName lcy::x64::kInstructionReturn         =   "ret";
InstructionName lcy::x64::kInstructionAdd            =   "add";
InstructionName lcy::x64::kInstructionSubtract       =   "sub";
InstructionName lcy::x64::kInstructionJmp            =   "jmp";
InstructionName lcy::x64::kInstructionCall           =   "call";
InstructionName lcy::x64::kInstructiomCmp            =   "cmp";
InstructionName lcy::x64::kInstructionSetE           =   "sete";
InstructionName lcy::x64::kInstructionSetNE          =   "setne";
InstructionName lcy::x64::kInstructionSetG           =   "setg";
InstructionName lcy::x64::kInstructionSetL           =   "setl";
InstructionName lcy::x64::kInstructionSetGE          =   "setge";
InstructionName lcy::x64::kInstructionSetLE          =   "setle";
InstructionName lcy::x64::kInstructinJE              =   "je";
InstructionName lcy::x64::kInstructinJG              =   "jg";
InstructionName lcy::x64::kInstructinJL              =   "jl";
InstructionName lcy::x64::kInstructionJNE            =   "jne";
InstructionName lcy::x64::kInstructinJGE             =   "jge";
InstructionName lcy::x64::kInstructinJLE             =   "jle";
InstructionName lcy::x64::kInstructionIMul           =   "imul";
InstructionName lcy::x64::kInstructionNeg            =   "neg";


/*=================================
 * Instructions
 *=================================
 */
Instruction::Instruction(OpSize size) : gSize(size)
{
}

std::string Instruction::getSizeSuffix() const
{
  std::string sizeSuffix = "";
  switch (gSize) {
    case OpSizeQuadWord:
      sizeSuffix = "q";
      break;
    case OpSizeDoubleWord:
      sizeSuffix = "l";
    default:
      break;
  }
  return sizeSuffix;
}

std::string NoOpInstruction::ToString() const
{
  return GetInstructionName();
}

OneOpInstruction::OneOpInstruction(OpSize size, std::shared_ptr<Operand> op)
:Instruction(size),
gOperand(op)
{
}

std::string OneOpInstruction::ToString() const
{
  
  return this->GetInstructionName() + getSizeSuffix() + " " + gOperand->ToString();
}

TwoOpInstruction::TwoOpInstruction( OpSize size, std::shared_ptr<Operand> op1, std::shared_ptr<Operand> op2)
:Instruction(size),
gOperandOne(op1),
gOperandTwo(op2)
{
}

std::string TwoOpInstruction::ToString() const
{
  return this->GetInstructionName() + getSizeSuffix() + " " + gOperandOne->ToString() + ", "  + gOperandTwo->ToString();
}

/*=================================
 * Directive
 *=================================
 */
Directive::Directive(std::string directive) : gDirective(directive)
{
}

std::string Directive::ToString() const
{
  return "." + gDirective;
}

/*=================================
 * Label
 *=================================
 */

Label::Label(std::string labelName) : gLabelName(labelName)
{
}

std::string Label::ToString() const
{
  return gLabelName + ":";
}

std::string Label::GetLabelName() const
{
  return gLabelName;
}

#endif
