#ifdef __x86_64
#include "x64assembly.h"

using namespace lcy;
using namespace x64;

X64Assembly::X64Assembly()
{
}

X64Assembly::~X64Assembly()
{
}

void X64Assembly::AddInstruction(std::shared_ptr<Instruction> instruction)
{
  addElement(instruction);
}

void X64Assembly::AddLabel(std::shared_ptr<Label> label)
{
  addElement(label);
}

void X64Assembly::AddLabel(std::string labelName)
{
  addElement(std::make_shared<Label>(labelName));
}

void X64Assembly::AddDirective(std::shared_ptr<Directive> directive)
{
  addElement(directive);
}

void X64Assembly::AddJmp(std::shared_ptr<JmpInstruction> jmpInstruction)
{
  addElement(jmpInstruction);
}

#endif
