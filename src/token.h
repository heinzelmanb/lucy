#pragma once

#include <string>

namespace lcy {

enum class TokenType
{
  None,
  Add,
  Subtract, 
  Multiply,
  Divide,
  Assign,
  Equals,
  NotEqual,
  LessThan,
  GreaterThan,
  Not,
  LessThanEqual,
  GreaterThanEqual,
  BitwiseAnd,
  BitwiseOr,
  LogicalAnd,
  LogicalOr,
  Id,
  Int32Keyword,
  Integer32,
  BeginKeyword,
  EndKeyword,
  FuncKeyword,
  Comma,
  LeftParen,
  RightParen,
  Tilde,
  Semicolon,
  DeclKeyword,
  ReturnKeyword,
  CallKeyword,
  IfKeyword,
  GotoKeyword,
  LeftBracket,
  RightBracket,
  At, // @
  Error,
  Eof
};

class Token 
{
public:
  Token(TokenType type);
	Token(TokenType type, int lineNumber);
  Token(TokenType type, std::string value, int lineNumber);
  Token(const Token &token);
  Token(Token &&token);

	TokenType GetType() const;
  
  std::string GetValue() const;
  
  bool IsOperator() const;
  
  bool IsRelationalOperator() const;
  
  bool IsPrefixOperator() const;
  
  int GetLineNumber() const;
  
  Token& operator=(const Token &other);
private:
	TokenType gType;
  std::string gValue;
  int gLineNumber;
};

}
