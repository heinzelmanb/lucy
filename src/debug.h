#pragma once

#include <cassert>
#include <iostream>

#define TODO() {\
  std::cerr << "Not implemented" << std::endl;\
  assert(0);\
}

#define FAIL(msg) {\
  std::cerr << msg << std::endl;\
  assert(0);\
}

