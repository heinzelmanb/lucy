#include "token.h"

using namespace lcy;

Token::Token(TokenType type)
: gType(type),
  gValue(""),
  gLineNumber(0)
{
}

Token::Token(TokenType type, int lineNumber)
:gType(type),
gValue(""),
gLineNumber(lineNumber)
{
}

Token::Token(TokenType type, std::string value, int lineNumber)
:gType(type),
gValue(value),
gLineNumber(lineNumber)
{
}

Token::Token(const Token &token)
{
  gType = token.GetType();
  gValue = token.GetValue();
  gLineNumber = token.GetLineNumber();
}

Token::Token(Token &&token)
{
  gType = token.GetType();
  gValue = token.GetValue();
  gLineNumber = token.GetLineNumber();
}

Token &Token::operator=(const Token &other)
{
  gType       =   other.GetType();
  gValue      =   other.GetValue();
  gLineNumber =   other.GetLineNumber();
  return *this;
}

TokenType Token::GetType() const 
{
  return gType;
}

std::string Token::GetValue() const
{
  return gValue;
}

int Token::GetLineNumber() const
{
  return gLineNumber;
}

bool Token::IsOperator() const
{
  switch (GetType()) {
    case TokenType::Add:
    case TokenType::Subtract:
    case TokenType::Multiply:
    case TokenType::Divide:
    case TokenType::LogicalOr:
    case TokenType::LogicalAnd:
    case TokenType::BitwiseOr:
    case TokenType::BitwiseAnd:
    case TokenType::LessThan:
    case TokenType::GreaterThan:
    case TokenType::GreaterThanEqual:
    case TokenType::LessThanEqual:
    case TokenType::Equals:
    case TokenType::NotEqual:
      return true;
    default:
      return false;
  }
}

bool Token::IsPrefixOperator() const
{
  switch (GetType()) {
    case TokenType::Subtract:
    case TokenType::Multiply:
    case TokenType::BitwiseAnd:
      return true;
    default:
      return false;
  }
}

bool Token::IsRelationalOperator() const
{
  switch (GetType()) {
    case TokenType::LogicalOr:
    case TokenType::LogicalAnd:
//    case TokenType::BitwiseOr:
//    case TokenType::BitwiseAnd:
    case TokenType::LessThan:
    case TokenType::GreaterThan:
    case TokenType::GreaterThanEqual:
    case TokenType::LessThanEqual:
    case TokenType::Equals:
    case TokenType::NotEqual:
      return true;
    default:
      return false;
  }
}
