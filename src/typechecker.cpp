#include "typechecker.h"
#include "ast.h"
#include "exceptions.h"

#include "debug.h"

using namespace lcy;

TypeChecker::TypeChecker()
{
}

bool TypeChecker::CheckType(Program *program)
{
  program->Accept(this);
  return true;
}

void TypeChecker::Visit(Program  *program)
{
  for (auto decl : program->GetDeclarationList()) {
    decl->Accept(this);
  }
}

void TypeChecker::Visit(FunctionDeclaration *functionDecl)
{
  gCurrentFunction = functionDecl;
  
  gDefinedLabels.clear();
  gLabelsInUse.clear();
  
  std::string functionId = functionDecl->GetFunctionIdentifier()->GetIdentifier();
  if (gFunctionLut.count(functionId) > 0) {
    throw TypeError("Redefintion of function " + functionId, 0);
  }

  gFunctionLut[functionId] = gCurrentFunction;

  gDefinedVars.Clear();
  
  if (functionDecl->GetParameterList()) {
    functionDecl->GetParameterList()->Accept(this);
  }
  
  if (functionDecl->GetStatementList()) {
    functionDecl->GetStatementList()->Accept(this);
  }
  
  // make sure all labels that have been used have also been defined.
  for (auto label : gLabelsInUse) {
    if (gDefinedLabels.count(label) == 0) {
      throw TypeError("Label '" + label + "' used without declaration", 0);
    }
  }
}

void TypeChecker::Visit(StatementList *statementList)
{
  for (auto statement : statementList->GetStatements()) {
    statement->Accept(this);
  }
}

void TypeChecker::Visit(IdentifierAddress *address)
{
  std::string id = address->GetIdentifier();
  VariableData *entry = gDefinedVars.GetDataForId(id);
  if (entry == nullptr) {
    throw TypeError(id + " was used before it was defined", 0);
  }
  
  if (address->GetOffset() == nullptr) {
    gLastType = entry->GetType()->Clone();
  } else {
    // must be an array.
    ArrayType *arrayType = static_cast<ArrayType *>((Type *)entry->GetType());
    gLastType = arrayType->GetBaseType()->Clone();
  }
}

void TypeChecker::Visit(AssignmentStatement *statement)
{
  std::string destination = statement->GetDestination()->GetIdentifier();
  VariableData *entry = gDefinedVars.GetDataForId(destination);
  if (entry == nullptr) {
    throw TypeError(destination + " was assigned before it was defined", 0);
  }
  
  if (entry->GetType()->IsStackArray() &&
      statement->GetDestination()->GetOffset() == nullptr) {
    throw TypeError("Stack arrays are not assignable", 0);
  }
  
  // make sure destination does not have prefix op if expression is complex
  if (statement->GetDestinationPrefixOperator() != TokenType::None &&
      statement->GetExpression()->IsComplex()) {
    throw TypeError("Destinations for complex assignments may not have prefix ops", 0);
  }
  
  if (statement->GetDestinationPrefixOperator() == TokenType::Multiply &&
      !entry->GetType()->IsReferenceType()) {
    throw TypeError("Can't dereference a non pointer", 0);
  }
  
  if (statement->GetDestination()->GetOffset() != nullptr && statement->GetDestinationPrefixOperator() != TokenType::None) {
    throw TypeError("Can't use prefix operator with offset", 0);
  }
  
  statement->GetExpression()->Accept(this);
  
  // check that the destination type matches the source type
  Type *assignedType = (Type *)entry->GetType();
  if (assignedType->IsReferenceType() && statement->GetDestinationPrefixOperator() == TokenType::Multiply) {
    assignedType = (Type *)static_cast<PointerType *>(assignedType)->GetBaseType();
  }
  if (assignedType->IsStackArray() && statement->GetDestination()->GetOffset() != nullptr) {
    assignedType = (Type *)static_cast<ArrayType *>(assignedType)->GetBaseType();
  }
  
  if (gLastType != nullptr && assignedType->IsEqual(gLastType.get()) == false) {
    throw TypeError("Source type does not equal destination type", 0);
  }
}

void TypeChecker::Visit(VariableInitializationStatement *statement)
{
  std::string id = statement->GetIdentifierAddress()->GetIdentifier();
  Type *type = statement->GetType();
  VariableData *entry = gDefinedVars.GetDataForId(id);
  
  if (entry != nullptr) {
    throw TypeError(id + " has already been defined", 0);
  }
  
  std::unique_ptr<VariableData> newEntry = MakeUnique<VariableData>(type);
  
  gDefinedVars.AddDataForId(id, std::move(newEntry));
  
  if (statement->GetType()->GetTypeSize() == 0 && statement->GetType()->IsStackArray()) {
    throw TypeError("Arrays must be initialized with valid size", 0);
  }
}

void TypeChecker::Visit(ReturnStatement *statement)
{
  Type *functionReturnType = gCurrentFunction->GetReturnType();
  
  if (statement->GetReturnAddress()) {
    statement->GetReturnAddress()->Accept(this);
  } else {
    gLastType = nullptr;
  }
  
  if (functionReturnType) {
    if (!functionReturnType->IsEqual(gLastType.get())) {
      throw TypeError("Return type for '" + gCurrentFunction->GetFunctionIdentifier()->GetIdentifier() + "' does not match", 0);
    }
  } else if (gLastType != nullptr) {
    throw TypeError("Return type for '" + gCurrentFunction->GetFunctionIdentifier()->GetIdentifier() + "' does not match", 0);
  }
}

void TypeChecker::Visit(CallStatement *statement)
{
  std::string functionId = statement->GetFunctionId()->GetIdentifier();
  if (gFunctionLut.count(functionId) == 0) {
    throw TypeError("Use of function " + functionId + " before definition", 0);
  }
  
  FunctionDeclaration *functionDecl = gFunctionLut[functionId];
  
  auto parameters = functionDecl->GetParameterList()->GetParameters();
  auto arguments = statement->GetArguments();
  
  if (parameters.size() != arguments.size()) {
    throw TypeError("Invalid argument count", 0);
  }
  
  for (int i = 0; i < parameters.size(); i++) {
    auto parameter = parameters[i];
    auto argument = arguments[i];
    
    argument->Accept(this);
    
    if (parameter->GetType()->IsEqual(gLastType.get()) == false) {
      throw TypeError("Invalid argument type", 0);
    }
    
    if (argument->IsSimpleAddress() == false) {
      throw TypeError("Function arguments must be simple.", 0);
    }
  }
  
  gLastType = functionDecl->GetReturnType()->Clone();
}

void TypeChecker::Visit(ParameterList *parameterList)
{
  for (auto param : parameterList->GetParameters()) {
    if (param->GetType()->IsStackArray()) {
      throw TypeError("Stack arrays cannot be arguments to functions, use a pointer instead", 0);
    }
    param->Accept(this);
  }
}

void TypeChecker::Visit(Int32Address *address)
{
  gLastType = PrimitiveType::IntType.Clone();
}

void TypeChecker::Visit(SimpleExpression *simpleExpression)
{
  simpleExpression->GetAddress()->Accept(this);
  
  if (simpleExpression->GetPrefixOperator() != TokenType::None) {
    if (simpleExpression->GetPrefixOperator() == TokenType::Subtract &&
        (gLastType->IsStackArray() || gLastType->IsReferenceType())) {
          throw TypeError("Invalid prefix operator for reference type", 0);
    }
    
    // dereferencing operator
    if (simpleExpression->GetPrefixOperator() == TokenType::Multiply) {
      if (gLastType->IsReferenceType() == false) {
        throw TypeError("Indirection requires pointer operand", 0);
      }
      
      PointerType *ptrType = static_cast<PointerType *>(gLastType.get());
      gLastType = ptrType->GetBaseType()->Clone();
    }
    
    if (simpleExpression->GetPrefixOperator() == TokenType::BitwiseAnd) {
      if (simpleExpression->GetAddress()->IsIdentifier() == false) {
        throw TypeError("Cannot take the address of an rvalue", 0);
      }
      
      gLastType = MakeUnique<PointerType>(gLastType->Clone());
    }
  }
}

void TypeChecker::Visit(ComplexExpression *complexExpression)
{
  if (complexExpression->GetLHS()->IsSimpleAddress() == false ||
      complexExpression->GetRHS()->IsSimpleAddress() == false) {
    throw TypeError("Complex expression must made of two simple addresses", 0);
  }
  
  complexExpression->GetLHS()->Accept(this);
  complexExpression->GetRHS()->Accept(this);
}

void TypeChecker::Visit(LucyLabel *label)
{
  std::string labelName = label->GetIdentifer()->GetIdentifier();
  if (gDefinedLabels.count(labelName) > 0) {
     throw TypeError(labelName + " has already been defined", 0);
  }
  gDefinedLabels.insert(label->GetIdentifer()->GetIdentifier());
}

void TypeChecker::Visit(IfStatement *statement)
{
  gLabelsInUse.insert(statement->GetLabel()->GetIdentifer()->GetIdentifier());
}

void TypeChecker::Visit(GotoStatement *statement)
{
  gLabelsInUse.insert(statement->GetLabel()->GetIdentifer()->GetIdentifier());
}
