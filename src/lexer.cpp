#include "lexer.h"

#include <cstdlib>

#include <iostream>

using namespace lcy;

Lexer::Lexer(std::shared_ptr<lcy::ProgramSource> input)
: gInput(input),
  gPeekToken(TokenType::None),
  gCurrentToken(TokenType::None)
{
}

Lexer::~Lexer()
{
}

const Token Lexer::PeekNextToken()
{
  if (gPeekToken.GetType() == TokenType::None) {
    gPeekToken = GetNextToken();
  }
  return gPeekToken;
}

const Token Lexer::GetCurrentToken()
{
  return gCurrentToken;
}
  
const Token Lexer::GetNextToken()
{
  if (gPeekToken.GetType() != TokenType::None) {
    auto tmp = gPeekToken;
    gPeekToken = TokenType::None;
    gCurrentToken = tmp;
    return tmp;
  }
  
  gCurrentToken = Token(TokenType::Error, getLineNumber());
  
  skipWs();
  
  if (gInput->Eof()) {
    gCurrentToken = Token(TokenType::Eof, getLineNumber());
  } else if (isdigit(peekChar())) {
    gCurrentToken = getIntegerToken();
  } else if (isalpha(peekChar())) {
    gCurrentToken = getTextToken();
  } else if (isOperator()) {
    gCurrentToken = getOperatorToken();
  } else if (isPunctuation()) {
    gCurrentToken = getPunctuationToken();
  }
  
  return gCurrentToken;
}

bool Lexer::isOperator()
{
  std::string peek = std::string(1, peekChar());
  return operatorMap.count(peek) > 0;
}

bool Lexer::isPunctuation()
{
  std::string peekString = std::string(1, peekChar());
  return punctuationMap.count(peekString) > 0;
}

const Token Lexer::getIntegerToken()
{
  std::string value = std::string(1, nextChar());
  while (isdigit(peekChar())) {
    value += nextChar();
  }
  return Token(TokenType::Integer32, value, getLineNumber());
}

const Token Lexer::getTextToken()
{
  std::string value = std::string(1, nextChar());
  while (isalnum(peekChar()) || peekChar() == '_') {
    value += nextChar();
    // check to see that next value is not alpha numeric to make sure
    // the keyword is not part of a larger identifier.
    if (keywordMap.count(value) > 0 && isalpha(peekChar()) == false) {
      TokenType type = keywordMap[value];
      return Token(type, getLineNumber());
    }
  }
  return Token(TokenType::Id, value, getLineNumber());
}

const Token Lexer::getOperatorToken()
{
  TokenType type = operatorMap[std::string(1, nextChar())];

  if (isOperator()) {
    TokenType peekOp = operatorMap[std::string(1, peekChar())];
    Token returnToken(TokenType::None);
    
    switch (type) {
      case TokenType::Assign:
        if (peekOp == TokenType::Assign) {
          returnToken = Token(TokenType::Equals, getLineNumber());
        }
        break;
      case TokenType::Not:
        if (peekOp == TokenType::Assign) {
          returnToken = Token(TokenType::NotEqual, getLineNumber());
        }
      case TokenType::BitwiseAnd:
        if (peekOp == TokenType::BitwiseAnd) {
           returnToken = Token(TokenType::LogicalAnd, getLineNumber());
        }
        break;
      case TokenType::BitwiseOr:
        if (peekOp == TokenType::BitwiseOr) {
           returnToken = Token(TokenType::LogicalOr, getLineNumber());
        }
        break;
      case TokenType::LessThan:
        if (peekOp == TokenType::Assign) {
           returnToken = Token(TokenType::LessThanEqual, getLineNumber());
        }
        break;
      case TokenType::GreaterThan:
        if (peekOp == TokenType::Assign) {
           returnToken = Token(TokenType::GreaterThanEqual, getLineNumber());
        }
      default:
        break;
    }
    if (returnToken.GetType() != TokenType::None) {
      nextChar();
      return returnToken;
    }
  }
  return Token(type, getLineNumber());
}

const Token Lexer::getPunctuationToken()
{
  TokenType type = punctuationMap[std::string(1, nextChar())];
  return Token(type, getLineNumber());
}

const char Lexer::nextChar() 
{
  return gInput->NextChar();
}

const char Lexer::peekChar() 
{
  return (const char)gInput->PeekChar();
}

void Lexer::skipWs() 
{
  gInput->SkipWhitespace();
}

int Lexer::getLineNumber() const
{
  return gInput->GetLineNumber();
}

std::map<std::string, TokenType> Lexer::operatorMap =
{
  {"+",   TokenType::Add},
  {"-",   TokenType::Subtract},
  {"*",   TokenType::Multiply},
  {"/",   TokenType::Divide},
  {">",   TokenType::GreaterThan},
  {"<",   TokenType::LessThan},
  {"|",   TokenType::BitwiseOr},
  {"&",   TokenType::BitwiseAnd},
  {"=",   TokenType::Assign},
  {"==",  TokenType::Equals},
  {"!=",  TokenType::NotEqual},
  {"!",   TokenType::Not},
  {"&&",  TokenType::LogicalAnd},
  {"||",  TokenType::LogicalOr},
  {">=",  TokenType::GreaterThanEqual},
  {"<=",  TokenType::LessThanEqual}
};

std::map<std::string, TokenType> Lexer::punctuationMap =
{
  {",",   TokenType::Comma},
  {"(",   TokenType::LeftParen},
  {")",   TokenType::RightParen},
  {"~",   TokenType::Tilde},
  {";",   TokenType::Semicolon},
  {"@",   TokenType::At},
  {"[",   TokenType::LeftBracket},
  {"]",   TokenType::RightBracket}
};

std::map<std::string, TokenType> Lexer::keywordMap =
{
  {"int32_t",   TokenType::Int32Keyword},
  {"end",       TokenType::EndKeyword},
  {"begin",     TokenType::BeginKeyword},
  {"func",      TokenType::FuncKeyword},
  {"decl",      TokenType::DeclKeyword},
  {"return",    TokenType::ReturnKeyword},
  {"call",      TokenType::CallKeyword},
  {"if",        TokenType::IfKeyword},
  {"goto",      TokenType::GotoKeyword}
};
