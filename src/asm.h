#pragma once

#include <memory>
#include <vector>
#include <string>

namespace lcy {
  
  // Element of an assembly program
  class AssemblyElement
  {
  public:
    AssemblyElement()
    {
    }
    
    virtual std::string ToString() const = 0;
    
  };
  
  // Base class for all elements of a program
  class Assembly
  {
  public:
    Assembly()
    {
    }
    
    ~Assembly()
    {
    }
    
    virtual std::string ToString() const {
      std::string str = "";
      for (auto element : gElements) {
        str += element->ToString() + "\n";
      }
      return str;
    }
    
  protected:
    std::vector<std::shared_ptr<AssemblyElement>> gElements;
    
  protected:
    void addElement(const std::shared_ptr<AssemblyElement> element) {
      gElements.push_back(element);
    }
  };
  
}




