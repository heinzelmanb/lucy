#include "ast.h"
#include "object.h"

#include <vector>
#include <cassert>

using namespace lcy;

/*------------------------------
 * program
 *------------------------------
 */

Program::Program()
{
}

Program::Program(Program &&other)
{
  gDeclList = std::move(other.gDeclList);
}

void Program::AppendDeclaration(std::unique_ptr<Declaration> decl)
{
  gDeclList.push_back(std::move(decl));
}

void Program::AppendDeclarationList(std::unique_ptr<Program> program)
{
  gDeclList.insert(gDeclList.end(),
             std::make_move_iterator(program->gDeclList.begin()),
             std::make_move_iterator(program->gDeclList.end())
             );
}

std::vector<Declaration *> Program::GetDeclarationList() const
{
  return GetPointerVector(gDeclList);
}

void Program::Accept(Visitor *visitor)
{
  visitor->Visit(this);
}

/*------------------------------
 * Function Declaration
 *-------------------------------
 */

FunctionDeclaration::FunctionDeclaration(std::unique_ptr<IdentifierAddress>  id,
                    std::unique_ptr<ParameterList>    parameterList,
                    std::unique_ptr<Type>             returnType,
                    std::unique_ptr<StatementList>    statementList) : Declaration()
{
  gId = std::move(id);
  gParameterList = std::move(parameterList);
  gReturnType = std::move(returnType);
  gStatementList = std::move(statementList);
}

FunctionDeclaration::FunctionDeclaration(FunctionDeclaration &&other)
: Declaration()
{
  gId = std::move(other.gId);
  gParameterList = std::move(other.gParameterList);
  gReturnType = std::move(other.gReturnType);
  gStatementList = std::move(other.gStatementList);
}

void FunctionDeclaration::Accept(Visitor *visitor)
{
  visitor->Visit(this);
}

StatementList *FunctionDeclaration::GetStatementList()
{
  return gStatementList.get();
}

IdentifierAddress *FunctionDeclaration::GetFunctionIdentifier() const
{
  return gId.get();
}

Type *FunctionDeclaration::GetReturnType() const
{
  return gReturnType.get();
}

ParameterList *FunctionDeclaration::GetParameterList() const
{
  return gParameterList.get();
}

/*------------------------------
 * Statement List
 *-------------------------------
 */
StatementList::StatementList()
{
}

StatementList::StatementList(StatementList &&other)
{
  gStatements = std::move(other.gStatements);
}

void StatementList::AppendStatement(std::unique_ptr<Statement> statement)
{
  gStatements.push_back(std::move(statement));
}

void StatementList::AppendStatements(std::unique_ptr<StatementList> statementList)
{
  gStatements.insert(gStatements.end(),
                   std::make_move_iterator(statementList->gStatements.begin()),
                   std::make_move_iterator(statementList->gStatements.end())
                   );
}

std::vector<Statement *> StatementList::GetStatements() const
{
  return GetPointerVector(gStatements);
}

void StatementList::Accept(Visitor *visitor)
{
  visitor->Visit(this);
}

/*------------------------------
 * Identifier Address
 *-------------------------------
 */
IdentifierAddress::IdentifierAddress(std::string identifier)
: gOffset(nullptr),
  gIdentifier(identifier)
{
}

IdentifierAddress::IdentifierAddress(std::string identifier, std::unique_ptr<Address> offset)
: gOffset(std::move(offset)),
  gIdentifier(identifier)
{
  
}

IdentifierAddress::IdentifierAddress(IdentifierAddress &&other)
{
  gIdentifier = other.gIdentifier;
}

std::string IdentifierAddress::GetIdentifier() const
{
  return gIdentifier;
}

const Address *IdentifierAddress::GetOffset() const
{
  return gOffset.get();
}

void IdentifierAddress::Accept(Visitor *visitor)
{
  visitor->Visit(this);
}

bool IdentifierAddress::IsEqual(Address *other) const
{
  bool equal = false;
  IdentifierAddress *otherId = dynamic_cast<IdentifierAddress *>(other);
  if (otherId) {
    equal = otherId->GetIdentifier() == GetIdentifier();
  }
  return equal;
}

bool IdentifierAddress::IsSimpleAddress() const
{
  return gOffset == nullptr;
}

/*------------------------------
 * Int Address
 *-------------------------------
 */
Int32Address::Int32Address(Int32 value) : gValue(value)
{
}

Int32 Int32Address::GetValue() const
{
  return gValue;
}

size_t Int32Address::GetSize() const
{
  return sizeof(Int32);
}

bool Int32Address::IsImmediateOperand() const
{
  return true;
}

void Int32Address::Accept(Visitor *visitor)
{
  visitor->Visit(this);
}

bool Int32Address::IsEqual(Address *other) const
{
  bool equal = false;
  Int32Address *otherInt = dynamic_cast<Int32Address *>(other);
  if (otherInt) {
    equal = otherInt->GetValue() == GetValue();
  }
  return equal;
}

/*------------------------------
 * Assignment Statement
 *-------------------------------
 */
AssignmentStatement::AssignmentStatement(std::unique_ptr<IdentifierAddress> destination,
                                         std::unique_ptr<Expression> expression,
                                         TokenType prefixOperator)
: gPrefixOperator(prefixOperator),
  gDestination(std::move(destination)),
  gExpression(std::move(expression))
{
}

AssignmentStatement::AssignmentStatement(AssignmentStatement &&other)
{
  gDestination = std::move(other.gDestination);
  gExpression = std::move(other.gExpression);
  
  other.gExpression = nullptr;
  other.gExpression = nullptr;
}

IdentifierAddress *AssignmentStatement::GetDestination() const
{
  return gDestination.get();
}

void AssignmentStatement::Accept(Visitor *visitor)
{
  visitor->Visit(this);
}

Expression *AssignmentStatement::GetExpression() const
{
  return gExpression.get();
}

TokenType AssignmentStatement::GetDestinationPrefixOperator() const
{
  return gPrefixOperator;
}

/*------------------------------
 * Variable Init
 *-------------------------------
 */
VariableInitializationStatement::VariableInitializationStatement(std::unique_ptr<Type> type,
                                                                 std::unique_ptr<IdentifierAddress> idAddr)
{
  gType = std::move(type);
  gIdentifierAddress = std::move(idAddr);
}

VariableInitializationStatement::VariableInitializationStatement(VariableInitializationStatement &&other)
{
  gType = std::move(other.gType);
  gIdentifierAddress = std::move(other.gIdentifierAddress);
  
  other.gIdentifierAddress = nullptr;
  other.gType = nullptr;
}

IdentifierAddress *VariableInitializationStatement::GetIdentifierAddress() const
{
  return gIdentifierAddress.get();
}

void VariableInitializationStatement::Accept(Visitor *visitor)
{
  visitor->Visit(this);
}

Type *VariableInitializationStatement::GetType() const
{
  return gType.get();
}

/*------------------------------
 * Primitive Type
 *-------------------------------
 */
PrimitiveType::PrimitiveType(TokenType type, size_t size)
{
  gType = type;
  gSize = size;
}

std::unique_ptr<Type> PrimitiveType::Clone() const
{
  return MakeUnique<PrimitiveType>(GetType(), GetTypeSize());
}

TokenType PrimitiveType::GetType() const
{
  return gType;
}

size_t PrimitiveType::GetTypeSize() const
{
  return gSize;
}

bool PrimitiveType::IsEqual(Type *other) const
{
  PrimitiveType *otherPrimitive = dynamic_cast<PrimitiveType *>(other);
  if (otherPrimitive != nullptr) {
    return gType == otherPrimitive->GetType() && gSize == otherPrimitive->GetTypeSize();
  }
  return false;
}

PrimitiveType PrimitiveType::IntType = PrimitiveType(TokenType::Int32Keyword, sizeof(int));

/*------------------------------
 * Array Type
 *-------------------------------
 */
ArrayType::ArrayType(std::unique_ptr<Type> baseType, size_t count)
: gBaseType(std::move(baseType)),
  count(count)
{
}

std::unique_ptr<Type> ArrayType::Clone() const
{
  return MakeUnique<ArrayType>(gBaseType->Clone(), count);
}

size_t ArrayType::GetTypeSize() const
{
  return count * gBaseType->GetTypeSize();
}

const Type *ArrayType::GetBaseType() const
{
  return gBaseType.get();
}

bool ArrayType::IsEqual(Type *other) const
{
  if (dynamic_cast<ArrayType *>(other) != nullptr) {
    return static_cast<ArrayType *>(other)->GetBaseType()->IsEqual((Type *)GetBaseType());
  }
  return false;
}

/*------------------------------
 * Pointer Type
 *-------------------------------
 */
PointerType::PointerType(std::unique_ptr<Type> baseType)
: gBaseType(std::move(baseType))
{
}

std::unique_ptr<Type> PointerType::Clone() const
{
  return MakeUnique<PointerType>(gBaseType->Clone());
}

size_t PointerType::GetTypeSize() const
{
  return sizeof(void *);
}

const Type *PointerType::GetBaseType() const
{
  return gBaseType.get();
}

bool PointerType::IsEqual(Type *other) const
{
  PointerType *otherPtr = dynamic_cast<PointerType *>(other);
  
  if (otherPtr != nullptr) {
    return GetBaseType()->IsEqual((Type *)otherPtr->GetBaseType());
  }
  return false;
}

/*------------------------------
 * Parameter List
 *-------------------------------
 */
ParameterList::ParameterList()
{
}

ParameterList::ParameterList(ParameterList &&other)
{
  gParameters = std::move(other.gParameters);
}

void ParameterList::AppendParameter(std::unique_ptr<VariableInitializationStatement> param)
{
  gParameters.push_back(std::move(param));
}

void ParameterList::AppendParameters(std::unique_ptr<ParameterList> params)
{
  if (params == nullptr) {
    return;
  }

  gParameters.insert(gParameters.end(),
                   std::make_move_iterator(params->gParameters.begin()),
                   std::make_move_iterator(params->gParameters.end())
                   );
}

std::vector<VariableInitializationStatement *> ParameterList::GetParameters() const
{
  return GetPointerVector(gParameters);
}

void ParameterList::Accept(Visitor *visitor)
{
  visitor->Visit(this);
}

/*------------------------------
 * Return Statement
 *-------------------------------
 */
ReturnStatement::ReturnStatement()
{
}

ReturnStatement::ReturnStatement(std::unique_ptr<Address> returnAddress)
: gReturnAddress(std::move(returnAddress))
{
}

ReturnStatement::ReturnStatement(ReturnStatement &&other)
{
  if (other.gReturnAddress) {
    gReturnAddress = std::move(other.gReturnAddress);
  }
  
  other.gReturnAddress = nullptr;
}

// optional address
Address *ReturnStatement::GetReturnAddress() const
{
  return gReturnAddress.get();
}

void ReturnStatement::SetReturnAddressValue(std::unique_ptr<Address> value)
{
  gReturnAddress = std::move(value);
}

void ReturnStatement::Accept(Visitor *visitor)
{
  visitor->Visit(this);
}

/*------------------------------
 * Call Statement
 *-------------------------------
 */
CallStatement::CallStatement(std::unique_ptr<IdentifierAddress> functionId)
: Expression(ExpressionType::FunctionCall),
  gFunctionId(std::move(functionId))
{
}

CallStatement::CallStatement(CallStatement &&other)
: Expression(std::move(other))
{
  gArguments = std::move(other.gArguments);
  gFunctionId = std::move(other.gFunctionId);
  
  other.gFunctionId = nullptr;
  other.gFunctionId = nullptr;
}

IdentifierAddress *CallStatement::GetFunctionId() const
{
  return gFunctionId.get();
}

std::vector<Address *> CallStatement::GetArguments() const
{
  return GetPointerVector(gArguments);
}

void CallStatement::AppendArguments(std::unique_ptr<Address> argument)
{
  gArguments.push_back(std::move(argument));
}

void CallStatement::Accept(Visitor *visitor)
{
  visitor->Visit(this);
}

/*------------------------------
 * Goto Statement
 *-------------------------------
 */

GotoStatement::GotoStatement(std::unique_ptr<LucyLabel> label)
:  gLabel(std::move(label))
{
}

GotoStatement::GotoStatement(GotoStatement &&other)
{
  gLabel = std::move(other.gLabel);
  other.gLabel = nullptr;
}

const LucyLabel *GotoStatement::GetLabel() const
{
  return gLabel.get();
}

void GotoStatement::Accept(Visitor *visitor)
{
  visitor->Visit(this);
}

/*------------------------------
 * Simple Expression
 *-------------------------------
 */

SimpleExpression::SimpleExpression(std::unique_ptr<Address> address)
: Expression(ExpressionType::Simple),
  gPrefixOperator(TokenType::None),
  gAddress(std::move(address))
{
}

SimpleExpression::SimpleExpression(std::unique_ptr<Address> address, TokenType prefixOperator)
: Expression(ExpressionType::Simple),
  gPrefixOperator(prefixOperator),
  gAddress(std::move(address))
{
}

SimpleExpression::SimpleExpression(SimpleExpression &&other)
: Expression(std::move(other))
{
  gAddress = std::move(other.gAddress);
  other.gAddress = nullptr;
}

Address *SimpleExpression::GetAddress() const
{
  return gAddress.get();
}

void SimpleExpression::Accept(Visitor *visitor)
{
  visitor->Visit(this);
}

TokenType SimpleExpression::GetPrefixOperator() const
{
  return gPrefixOperator;
}


/*------------------------------
 * Complex Expression
 *-------------------------------
 */
ComplexExpression::ComplexExpression(std::unique_ptr<Address> left,
                                     std::unique_ptr<Address> right,
                                     TokenType                op)
: Expression(ExpressionType::Complex),
gOp(op),
gLeftAddress(std::move(left)),
gRightAddress(std::move(right))
{
  // complex expressions are made up of two SIMPLE addresses.
  //assert(gLeftAddress->IsSimpleAddress() && gRightAddress->IsSimpleAddress());
}

ComplexExpression::ComplexExpression(ComplexExpression &&other)
: Expression(std::move(other))
{
  gOp = other.gOp;
  gLeftAddress = std::move(other.gLeftAddress);
  gRightAddress = std::move(other.gRightAddress);
  
  other.gLeftAddress = nullptr;
  other.gRightAddress = nullptr;
}

Address *ComplexExpression::GetLHS() const
{
  return gLeftAddress.get();
}

Address *ComplexExpression::GetRHS() const
{
  return gRightAddress.get();
}

TokenType ComplexExpression::GetOp()  const
{
  return gOp;
}

void ComplexExpression::Accept(Visitor *visitor)
{
  visitor->Visit(this);
}


LucyLabel::LucyLabel(std::unique_ptr<IdentifierAddress> identifier)
: gLabelIdentifier(std::move(identifier))
{
}

LucyLabel::LucyLabel(LucyLabel &&other)
{
  gLabelIdentifier = std::move(other.gLabelIdentifier);
  other.gLabelIdentifier = nullptr;
}

IdentifierAddress *LucyLabel::GetIdentifer() const
{
  return gLabelIdentifier.get();
}

void LucyLabel::Accept(Visitor *visitor)
{
  visitor->Visit(this);
}

/*------------------------------
 * If Statement
 *-------------------------------
 */

IfStatement::IfStatement(std::unique_ptr<Expression> expression,
                         std::unique_ptr<LucyLabel> label)
: gExpression(std::move(expression)),
gLabel(std::move(label))
{
}

IfStatement::IfStatement(IfStatement &&other)
{
  gExpression = std::move(other.gExpression);
  gLabel = std::move(other.gLabel);
  
  gLabel = nullptr;
  gExpression = nullptr;
}

Expression *IfStatement::GetExpression() const
{
  return gExpression.get();
}

LucyLabel *IfStatement::GetLabel() const
{
  return gLabel.get();
}

void IfStatement::Accept(Visitor *visitor)
{
  visitor->Visit(this);
}

